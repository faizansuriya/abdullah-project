import React, { Component } from "react";
import { View, TouchableOpacity, Text } from "react-native";
import * as userUtil from "../../utils/user.util";
import * as auth from "../../utils/auth.util";
import { NavigationActions } from "react-navigation";
export default class LoginFirstComp extends Component {

  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          backgroundColor: "white",
          alignItems: "center"
        }}
      >
        <Text> You have to log-in first for this screen. </Text>
      </View>
    );
  }
}
