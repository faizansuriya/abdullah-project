import React, { Component } from "react";
import { withNavigationFocus } from "react-navigation";
import { View } from "react-native";
import { Text, Button } from "../ui";

import ThanksForReview from "./ThanksForReview";
import GiveReview from "./GiveReview";
import StatusButton from "./StatusButton";
const statusCompleted = "completed";

const StatusComp = ({ data = {}, handleGiveReview }) => {
  const { actStatus, review = null } = data;
  if (actStatus === statusCompleted) {
    return review ? (
      <ThanksForReview data={review} />
    ) : (
      <GiveReview data={data} onPress={handleGiveReview} />
    );
  } else return <StatusButton data={data} />;
};
const OrderRow = ({ item = {}, handleGiveReview }) => {
  const { title, description } = item;
  return (
    <View padder style={style.row}>
      <View padder style={style.controlBlock}>
        <StatusComp data={item} {...{ handleGiveReview }} />
      </View>
      <View padder style={style.textBlock}>
        <Text
          style={[style.alignRight, style.title]}
          weight={"bold"}
          level={"large"}
          density={"normal"}
        >
          {title}
        </Text>
        <Text style={[style.alignRight]}>{description}</Text>
      </View>
    </View>
  );
};

const style = {
  row: {
    borderBottomWidth: 1,
    borderBottomColor: "#dedbe3",
    padding: 15,
    flex: 1,
    flexDirection: "row",
   
  },
  textBlock: { flex: 8 },
  controlBlock: {
    flex: 4,
    padding: 5,
  
  },
  title: {
    color: "black"
  },
  alignRight: { textAlign: "right" }
};

export default OrderRow;
