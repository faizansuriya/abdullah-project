import React from "react";
import { TouchableOpacity, View } from "react-native";
import { Text, Button, Stars } from "../ui";
import locale from "../../resources/strings/ar";

const GiveReview = ({ data = {}, onPress }) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        borderWidth: 1,
        borderColor: "#ff0000",
        padding: 10
      }}
    >
      <View>
        <View style={{ alignItems: "center", margin: 5 }}>
          <Stars value={0} iconSize={12} editable={false} />
        </View>
        <Text
          weight={"bold"}
          level={"normal"}
          density={"tight"}
          style={{ color: "black", textAlign: "center" }}
        >
          {locale.general.giveReview}
        </Text>
      </View>
    </TouchableOpacity>
  );
};
export default GiveReview;
