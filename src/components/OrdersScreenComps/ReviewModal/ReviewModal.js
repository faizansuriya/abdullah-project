import React, { Component } from "react";
import { Modal, View, Alert, KeyboardAvoidingView } from "react-native";

import { Text, Button, TextInput, Label, Stars } from "../../../components/ui";

import styles from "./reviewModalStyle.js";
import * as generalStyles from "../../../styles/generalStyle";
import locale from "../../../resources/strings/ar";

import services from "../../../services";

class ReviewModal extends Component {
  state = {
    speed: "",
    star: 0,
    description: ""
  };

  _handleSubmit = async () => {
    const { onSubmit, orderId } = this.props;
    const postBody = { ...this.state, orderId };
    if (!this._validatePost(postBody))
      Alert.alert("تنبيه", "Please filled the review form completely.");
    else {
      const review = await services.review.create(postBody);
      onSubmit && onSubmit(orderId, review);
    }
  };

  _validatePost({ speed, description, star }) {
    return (
      speed && speed !== "" && star > 0 && description && description !== ""
    );
  }

  _handleOnChange = (key, val) => {
    this.setState(() => ({ [key]: val }));
  };
  render() {
    const { speed, star, description } = this.state;
    const { onClose } = this.props;

    return (
      <View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={true}
          onRequestClose={onClose}
        >
          <View style={styles.backDrop}>
            <View style={styles.container}>
              <KeyboardAvoidingView
                style={{ backgroundColor: "white" }}
                behavior={"position"}
                keyboardVerticalOffset={0}
              >
                <View>
                  <Text level={"huge"} weight={"bold"} style={styles.title}>
                    {locale.review.title}
                  </Text>
                  <Text style={styles.subTitle}>{locale.review.subTitle}</Text>
                </View>
                <View style={styles.ratingContainer}>
                  <Stars
                    value={star}
                    onChange={this._handleOnChange.bind(this, "star")}
                  />
                </View>
                <View style={generalStyles.form.style}>
                  <View regular style={[generalStyles.controlMargin.style]}>
                    <Text style={styles.inputControlLabel}>
                      {locale.review.speed}
                    </Text>
                    <TextInput
                      containerStyle={styles.inputControlContainer}
                      style={[styles.inputControl, { height: 50 }]}
                      value={speed}
                      placeholder={locale.review.speedPh}
                      onChangeText={this._handleOnChange.bind(this, "speed")}
                    />
                  </View>

                  <View regular style={[generalStyles.controlMargin.style]}>
                    <Text style={styles.inputControlLabel}>
                      {locale.review.description}
                    </Text>
                    <TextInput
                      multiline={true}
                      containerStyle={styles.inputControlContainer}
                      style={[
                        styles.inputControl,
                        { height: 110, textAlignVertical: "top" }
                      ]}
                      value={description}
                      placeholder={locale.review.descriptionPh}
                      onChangeText={this._handleOnChange.bind(
                        this,
                        "description"
                      )}
                    />
                  </View>

                  <Button
                    containerStyle={{
                      marginVertical: 20,
                      marginRight: 10,
                      marginLeft: 10
                    }}
                    block
                    onPress={this._handleSubmit}
                    label={locale.review.submit}
                    labelStyle={{ fontSize: 32, fontWeight: "bold" }}
                  />
                </View>
              </KeyboardAvoidingView>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

export default ReviewModal;
