import { StyleSheet } from "react-native";

export default StyleSheet.create({
  backDrop: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0, 0, 0, 0.8)"
  },

  container: {
    backgroundColor: "white",
    margin: 20,
    padding: 10
  },
  title: {
    textAlign: "center",
    color: "black"
  },
  subTitle: {
    textAlign: "center",
    justifyContent: "center"
  },
  inputControl: {
    borderWidth: 1,
    borderColor: "lightgrey",
    textAlign: "right"
  },
  inputControlContainer: {
    borderBottomWidth: 0
  },
  inputControlLabel: { color: "black", textAlign: "right" },
  ratingContainer: {
    alignItems: "center",
    justifyContent: "center",
    margin: 20
  }
});
