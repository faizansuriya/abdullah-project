import React from "react";
import { View } from "react-native";
import { Text } from "../ui";

const StatusButton = ({ data }) => {
  const { status, actStatus } = data;
  const _btnClass = style[`btn_${actStatus}`] || {};
  return (
    <View style={_btnClass}>
      <Text style={{ textAlign: "center", padding: 4, color: "white" }}>
        {status}
      </Text>
    </View>
  );
};

const style = {
  btn_processing: { backgroundColor: "#f71e66" },
  btn_wfmt: { backgroundColor: "#9f00ad" }
};
export default StatusButton;
