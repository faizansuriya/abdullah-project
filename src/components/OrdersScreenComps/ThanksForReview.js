import React from "react";
import { View } from "react-native";
import { Text, Button, Stars } from "../ui";
import locale from "../../resources/strings/ar";

const ThanksForReview = ({ data = {} }) => {
  const { star = 0 } = data;
  return (
    <View
      style={{
        borderWidth: 1,
        borderColor: "#dedbe3",
        padding: 3
      }}
    >
      <View style={{ alignItems: "center", padding: 5 }}>
        <Stars value={star} iconSize={12} editable={false} />
      </View>
      <Text
        weight={"bold"}
        level={"normal"}
        density={"tight"}
        style={{ color: "black", textAlign: "center", padding : 5 }}
      >
        {locale.general.thanksForReview}
      </Text>
    </View>
  );
};
export default ThanksForReview;
