import React, { Component } from "react";
import { withNavigationFocus } from "react-navigation";
import { View, FlatList, Alert } from "react-native";
import { Text } from "../../ui";
import services from "../../../services";
import OrderRow from "../OrderRow";
import locale from "../../../resources/strings/ar";
import { requestParseError } from "../../../utils/general.util";

import _ from "lodash";

function resTransformer(row, i) {
  return {
    key: `key-${i}`,
    title: locale.services[row.type],
    description: row.description,
    actStatus: row.status,
    status: locale.statuses[row.status]
  };
}
class CurrentOrderTab extends Component {
  state = {
    currentOrders: []
  };
  async componentWillReceiveProps(nextProps) {
    const _forFirstTimeActive = nextProps.isFocused && this.props.isFocused;
    const _forEveryTimeActive = nextProps.isFocused !== this.props.isFocused;
    if (_forFirstTimeActive || _forEveryTimeActive) {
      try {
        const data = await this.fetchCurrentOrders();
        this.setState(() => ({ currentOrders: data }));
      } catch (ex) {
        // const _msg = requestParseError(ex);
        // Alert.alert("تنبيه", _msg);
      }
    }
  }

  async fetchCurrentOrders() {
    const _filters = `status=processing%26status=wfmt`;
    const reqParams = `filter=${_filters}&offset=0&limit=10`;
    const response = await services.order.getOrders(reqParams);
    const { pageContext, records } = response;
    const data = _.map(records, resTransformer);
    return data;
  }

  render() {
    const { currentOrders } = this.state;
    return (
      <FlatList
        data={currentOrders}
        renderItem={OrderRow}
        style={{ padding: 10, backgroundColor: "white" }}
      />
    );
  }
}
export default withNavigationFocus(CurrentOrderTab);
