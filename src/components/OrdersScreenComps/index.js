import CurrentOrdersTab from "./CurrentOrdersTab";
import CompletedOrdersTab from "./CompletedOrdersTab";

export { CurrentOrdersTab, CompletedOrdersTab };
