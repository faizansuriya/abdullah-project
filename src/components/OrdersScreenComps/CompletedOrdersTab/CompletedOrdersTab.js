import React, { Component } from "react";
import { withNavigationFocus } from "react-navigation";
import { View, FlatList, Alert } from "react-native";
import { Text } from "../../ui";
import services from "../../../services";
import OrderRow from "../OrderRow";
import ReviewModal from "../ReviewModal";
import locale from "../../../resources/strings/ar";
import { requestParseError } from "../../../utils/general.util";

import _ from "lodash";
function resTransformer(row, i) {
  return {
    id: row.id,
    key: `key-${i}`,
    title: locale.services[row.type],
    description: row.description,
    status: locale.statuses[row.status],
    actStatus: row.status,
    review: row.review || null
  };
}
class CompletedOrderTab extends Component {
  state = {
    completedOrders: [],
    orderSelectedForReview: null
  };
  async componentWillReceiveProps(nextProps) {
    const _forFirstTimeActive = nextProps.isFocused && this.props.isFocused;
    const _forEveryTimeActive = nextProps.isFocused !== this.props.isFocused;
    if (_forFirstTimeActive || _forEveryTimeActive) {
      try {
        const data = await this.fetchCompletedOrders();
        this.setState(() => ({ completedOrders: data }));
      } catch (ex) {
        // const _msg = requestParseError(ex);
        // Alert.alert("تنبيه", _msg);
      }
    }
  }

  _handleSubmitReview = (orderId, review) => {
    this.setState(ps => {
      const _transfomed = _.clone(ps.completedOrders);
      const _index = _.findIndex(_transfomed, { id: orderId });
      _.set(_transfomed, `${_index}.review`, review);
      return {
        completedOrders: _transfomed,
        orderSelectedForReview: null
      };
    });
  };

  _handleGiveReview = id => {
    this.setState(() => ({ orderSelectedForReview: id }));
  };

  async fetchCompletedOrders() {
    const _filters = `status=completed`;
    const reqParams = `filter=${_filters}&offset=0&limit=10`;
    const response = await services.order.getOrders(reqParams);
    const { pageContext, records } = response;
    const data = _.map(records, resTransformer);
    return data;
  }

  _renderItem = props => {
    return (
      <OrderRow
        {...props}
        handleGiveReview={() => {
          this._handleGiveReview(props.item.id);
        }}
      />
    );
  };

  render() {
    const { completedOrders, orderSelectedForReview } = this.state;

    return (
      <View style={{ flex: 1 }}>
        <FlatList
          data={completedOrders}
          renderItem={this._renderItem}
          style={{ padding: 10, backgroundColor: "white" }}
        />
        {!orderSelectedForReview ? null : (
          <ReviewModal
            onClose={() => {
              this._handleGiveReview(null);
            }}
            onSubmit={this._handleSubmitReview}
            orderId={orderSelectedForReview}
          />
        )}
      </View>
    );
  }
}
export default withNavigationFocus(CompletedOrderTab);
