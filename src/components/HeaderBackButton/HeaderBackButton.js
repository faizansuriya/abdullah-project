import _ from "lodash";
import React from "react";
import { StyleSheet, TouchableHighlight, View } from "react-native";
// import Icon from "react-native-vector-icons/FontAwesome";

import { Text } from "../ui";

const HeaderBackButton = props => {
  const { goBack } = props.navigation;
  return (
    <TouchableHighlight onPress={goBack}>
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <Text>o </Text>
      </View>
    </TouchableHighlight>
  );
};

export default HeaderBackButton;
