import React, { Component } from "react";
import { View, TouchableOpacity, Text } from "react-native";
import * as userUtil from "../../utils/user.util";
import * as auth from "../../utils/auth.util";
import LoginFirmComp from "../LoginFirstComp";
import { NavigationActions } from "react-navigation";

export default class LoginCheckHOC extends Component {
  state = { isLoggedIn: null };

  async componentDidMount() {
    const isLoggedIn = await auth.isLoggedIn();
    this.setState({ isLoggedIn });
  }

  async componentWillReceiveProps(nextProps) {
    if (!nextProps.isFocused) return;
    const { LoginComp } = nextProps;
    const isLoggedIn = await auth.isLoggedIn();
    if (!isLoggedIn && !LoginComp) {
      nextProps.navigation.navigate({ routeName: "LoginWithReset" });
    } else this.setState({ isLoggedIn });
  }
  render() {
    const { isLoggedIn } = this.state;
    const { children, LoginComp } = this.props;
    if (isLoggedIn === null) return null;
    if (isLoggedIn) return children;
    if (LoginComp) return LoginComp;
    return <LoginFirmComp />;
  }
}
