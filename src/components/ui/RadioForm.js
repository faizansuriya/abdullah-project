import React, { Component } from "react";
import { Text } from "../ui";
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel
} from "react-native-simple-radio-button";
import moment from "moment";
import "moment/locale/ar";

const buttonInnerColor = "#f8b916";
const buttonOuterColor = "#00ada9";
const labelColor = "#4e4d50";

class RadioFormWrap extends Component {
  state = {
    selected: this.props.selected
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.selected !== this.props.selected)
      this.setState(() => ({ selected: nextProps.selected }));
  }
  render() {
    const { data, onPress } = this.props;
    const { selected } = this.state;
    const { extraText, extraTextShow, extraTextWith } = this.props;
    moment.locale("ar");
    let text = moment(extraText).format("LLLL");

    return (
      <RadioForm
        formHorizontal={false}
        animation={true}
        style={{ alignItems: "flex-end", margin: 10 }}
      >
        {data.map((obj, i) => {
          const isSelected = selected === obj.value;
          const extraInfo =
            !extraTextShow || obj !== extraTextWith ? null : (
              <Text style={{ position: "absolute", right: 25 }}>
                {" "}
                {"\n\n\n (" + text})
              </Text>
            );
          return (
            <RadioButton
              key={i}
              buttonStyle={{
                height: 10,
                width: 10,
                backgroundColor: "red",
                borderRadius: "0"
              }}
            >
              {extraInfo}

              <RadioButtonLabel
                obj={obj}
                index={i}
                onPress={onPress}
                labelStyle={{
                  fontSize: 12,
                  color: labelColor,
                  textAlign: "right",
                  margin: 3,
                  fontFamily: "Neo Sans Arabic"
                }}
                labelWrapStyle={{}}
              />

              <RadioButtonInput
                obj={obj}
                index={i}
                isSelected={isSelected}
                onPress={onPress}
                borderWidth={2}
                buttonInnerColor={buttonInnerColor}
                buttonOuterColor={buttonOuterColor}
                buttonSize={10}
                buttonOuterSize={20}
                buttonWrapStyle={{ marginLeft: 5 }}
              />
            </RadioButton>
          );
        })}
      </RadioForm>
    );
  }
}

export default RadioFormWrap;
