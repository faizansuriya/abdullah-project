import React from "react";
import Text from "./Text";
import { addFontStyle } from "./addFontStyle";

// The only currently supported format is [BOLD]Bold text[/BOLD].
export default ({ text, ...props }) => {
  let currentText = text;
  let startIndex = currentText.indexOf("[BOLD]");

  const components = [];
  // if (startIndex < 0) {
  //   return <Text {...props}>{text}</Text>;
  // }

  let key = 1;
  while (startIndex >= 0) {
    if (startIndex > 0) {
      let normalText = currentText.substring(0, startIndex);
      components.push(normalText);

      currentText = currentText.substring(startIndex);
      startIndex = 0;
    }

    let endIndex = currentText.indexOf("[/BOLD]");
    if (endIndex < 0) {
      console.error("Invalid formatted text specified", text);
      return null;
    }

    endIndex += 7;

    let boldText = currentText.substring(startIndex + 6, endIndex - 7);
    components.push(
      <Text weight="bold" key={++key}>
        {boldText}
      </Text>
    );

    currentText = currentText.substring(endIndex);
    startIndex = currentText.indexOf("[BOLD]");
  }

  if (currentText.length > 0) {
    components.push(currentText);
  }

  return <Text {...props}>{components}</Text>;
};
