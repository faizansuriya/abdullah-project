import _ from "lodash";
import React from "react";
import { StyleSheet, TouchableHighlight, View } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";

import { Text } from "../ui";

const buttonStyles = styles =>
  StyleSheet.create(
    _.defaultsDeep(styles, {
      touchable: {
        borderRadius: 5,
        paddingHorizontal: 15,
        paddingVertical: 10,
        backgroundColor: "black"
      },
      icon: {
        fontSize: 18,
        paddingHorizontal: 5,
        color: "white"
      },
      label: {
        fontSize: 15,
        color: "white",
        textAlign: "center"
      }
    })
  );

const normalStyles = buttonStyles({
  touchable: {
    backgroundColor: "#4E4D50",
    borderWidth: 1,
    borderColor: "white"
  },
  touchableActive: {
    backgroundColor: "rgba(30,28,26, .2)"
  },
  label: {
    color: "white"
  }
});

const submitStyles = buttonStyles({
  touchable: {
    backgroundColor: "white"
  },
  touchableActive: {
    backgroundColor: "rgba(0, 0, 0, .2)"
  },
  label: {
    color: "#00caca"
  }
});

const Button = ({
  icon,
  label,
  underlayColor,
  containerStyle,
  iconStyle,
  labelStyle,
  labelWeight,
  labelLevel,
  type,
  rightIcon,
  rightIconStyle,
  onPress,
  ...props
}) => {
  const styles = type == "submit" ? submitStyles : normalStyles;
  underlayColor =
    underlayColor || StyleSheet.flatten(styles.touchableActive).backgroundColor;

  return (
    <TouchableHighlight
      style={[styles.touchable, containerStyle]}
      underlayColor={underlayColor}
      activeOpacity={1}
      onPress={onPress}
    >
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        {icon && <Icon name={icon} style={[styles.icon, iconStyle]} />}
        {label && (
          <Text
            style={[styles.label, labelStyle]}
            weight={labelWeight || "medium"}
            level = {labelLevel}
          >
            {label.toUpperCase()}
          </Text>
        )}
        {rightIcon && (
          <Icon name={rightIcon} style={[styles.icon, rightIconStyle]} />
        )}
      </View>
    </TouchableHighlight>
  );
};

export default Button;
