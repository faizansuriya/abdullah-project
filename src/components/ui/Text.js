import React from "react";
import { Text } from "react-native";
import { addFontStyle } from "./addFontStyle";

export default addFontStyle(Text);
