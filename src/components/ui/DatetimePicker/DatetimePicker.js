import React, { Component } from "react";
import _ from "lodash";
import { Text, TouchableOpacity, View } from "react-native";
import DateTimePicker from "react-native-modal-datetime-picker";
import locale from "../../../resources/strings/ar";

class DatetimePicker extends Component {
  render() {
    return (
      <DateTimePicker
        // confirmTextStyle
        // titleStyle
        // cancelTextStyle
        cancelTextIOS={locale.general.cancel}
        titleIOS={locale.general.datePickerTitle}
        confirmTextIOS={locale.general.confirm}
        mode={"datetime"}
        isVisible={this.props.isVisible}
        onConfirm={this.props.onConfirm}
        onCancel={this.props.onCancel}
      />
    );
  }
}

export default DatetimePicker;
