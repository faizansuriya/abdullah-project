import React from "react";
import { View, TextInput, Image } from "react-native";
import { addFontStyle } from "./addFontStyle";
import { inputStyle } from "../../styles/generalStyle";

const Input = props => {
  const _containerStyle = props.containerStyle || inputStyle.container;
  const _props = {
    style: props.style || inputStyle.style,
    underlineColorAndroid: "transparent",
    ...props
  };
  return (
    <View style={ [_containerStyle]}>
      <TextInput {..._props}>{props.children}</TextInput>
    </View>
  );
};
export default Input;
