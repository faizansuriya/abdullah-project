import React from "react";
import { CachedImage as RNCachedImage } from "react-native-cached-image";

export default function CachedImage(props) {
  if (!props.source) {
    return null;
  }
  return <RNCachedImage {...props} useQueryParamsInCacheKey={true} />;
}
