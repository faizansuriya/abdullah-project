import React from "react";
import PropTypes from "prop-types";

const FontFamilies = {
  normal: "Neo Sans Arabic",
  medium: "NeoSansW23-Medium",
  bold: "NeoSansW23-Medium"
};

const FontSizes = {
  tiny: 8,
  small: 10,
  normal: 12,
  large: 14,
  extra_large: 16,
  huge: 22,
  XXL: 46
};

const FontDensity = {
  tight: 1,
  normal: 1.4,
  loose: 1.8
};

export function addFontStyle(Component) {
  return class FontStyleComponent extends React.Component {
    static defaultProps = {
      weight: "normal",
      level: "normal",
      density: "normal"
    };

    render() {
      const {
        children,
        style,
        weight,
        level,
        density,
        nativeRef,
        forceSize,
        ...props
      } = this.props;
      const fontFamily = FontFamilies[weight];
      const fontSize = forceSize || FontSizes[level];
      const lineHeight = fontSize * FontDensity[density];

      return (
        <Component
          ref={nativeRef}
          style={[style, { fontFamily, fontSize, lineHeight }]}
          {...props}
        >
          {children}
        </Component>
      );
    }
  };

  FontStyleComponent.propTypes = {
    weight: PropTypes.oneOf(["normal", "medium", "bold"]),
    level: PropTypes.oneOf([
      "tiny",
      "small",
      "normal",
      "large",
      "extra_large",
      "huge",
      "XXL"
    ]),
    density: PropTypes.oneOf(["tight", "normal", "loose"])
  };
}
