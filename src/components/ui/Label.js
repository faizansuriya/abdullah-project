import React from "react";
import { Text } from "./index";
import { labelStyle } from "../../styles/generalStyle";
const Label = props => {
  return (
    <Text
      {...props}
      level={props.level}
      style={[labelStyle.style, props.style]}
    >
      {props.children}
    </Text>
  );
};
export default Label;
