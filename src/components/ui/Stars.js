import React, { Component } from "react";
import { Modal, View } from "react-native";

import Rating from "react-native-easy-rating";
const filledStarIcon = require("../../resources/assets/icons/general/star-fill.png");
const starIcon = require("../../resources/assets/icons/general/star.png");

const Stars = props => {
  const { editable = true, iconSize = 36, value = 0 } = props;
  return (
    <Rating
      rating={value}
      max={5}
      iconWidth={iconSize}
      iconHeight={iconSize}
      iconSelected={filledStarIcon}
      iconUnselected={starIcon}
      onRate={props.onChange}
      editable={editable}
    />
  );
};

export default Stars;
