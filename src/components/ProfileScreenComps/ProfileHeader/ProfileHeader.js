import React from "react";
import { View, TouchableHighlight, Image } from "react-native";
import { Text } from "../../ui";
import style from "./profileHeaderStyle";

import editImage from "../../../resources/assets/icons/general/edit.png";
const ProfileHeader = props => {
  const { data, onEdit = null } = props;
  const { name = "..." } = data;
  return (
    <View style={style.container}>
      <View style={style.headerStyle}>
        <View style={style.editButtonContainer} />
      </View>
      { (
        <TouchableHighlight
          underlayColor={"transparent"}
          activeOpacity={0.7}
          onPress={onEdit}
          style={style.editButton}
        >
          <Image source={editImage} style={style.editBtnImage} />
        </TouchableHighlight>
      )}

      <View style={{ position: "absolute", top: 50, right: 0, left: 0 }}>
        <Text style={style.userName} weight="bold" level="huge" density="loose">
          {name}
        </Text>
      </View>
    </View>
  );
};
export default ProfileHeader;
