import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    height: 150,
  },
  editButtonContainer: {
    alignItems: "flex-end"
  },
  editButton: {
    position: "absolute",
    right: 3,
    paddingRight: 30,
    paddingTop: 30
  },
  editBtnImage: {
    height: 30,
    width: 30
  },

  headerStyle: { marginLeft: 60,
    marginTop: -10,
    width: 250,
    height: 150,
    borderRadius: 250,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    backgroundColor: "#f2f1ee",
    borderWidth: 1,
    borderColor: "#f2f1ee",
    transform: [{ scaleX: 2 }]},

  userName: {
    color: "black",
    justifyContent: "center",
    alignSelf: "center"
  }
});
