import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flexDirection: "column",
    padding: 10,
   
  },
  seperator : {
    marginTop : 10,
    borderTopWidth: 2,
    borderTopColor: "#ebebeb",
    marginBottom: 5
  },
  row: {
    marginBottom : 10,
  },
  label: {
    marginBottom: 5,
    marginTop: 2
  }
});
