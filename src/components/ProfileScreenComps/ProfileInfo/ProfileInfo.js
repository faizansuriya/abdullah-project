import React from "react";

import { Switch, View, TouchableHighlight } from "react-native";

import style from "./profileInfoStyle";

import { Text, Button, TextInput, Label } from "../../ui";
const ProfileInfo = props => {
  const { locale, data } = props;
  return (
    <View style={style.container}>
      <View style={style.seperator} />
      <View regular style={style.row}>
        <Label level="large" style={style.label}>{locale.profile.email}</Label>
        <TextInput autoFocus value={data.email} editable={false} />
      </View>

      <View regular style={style.row}>
        <Label level="large" style={style.label}>{locale.profile.mobile}</Label>
        <TextInput autoFocus value={data.mobile} editable={false} />
      </View>

      <View regular style={style.row}>
        <Label level="large" style={style.label}>{locale.profile.city}</Label>
        <TextInput autoFocus value={data.city} editable={false} />
      </View>
    </View>
  );
};
export default ProfileInfo;
