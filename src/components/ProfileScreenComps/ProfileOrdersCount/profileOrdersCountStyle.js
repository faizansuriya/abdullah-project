import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  orderContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 5
  },
  orderSeparator: {
    borderRightWidth: 2,
    borderRightColor: "#f2f1ee"
  },
  orderCount: {
    color: "black"
  },
  orderLabel: {
    color: "#00ada9"
  }
});
