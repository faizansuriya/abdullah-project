import React from "react";
import { View, TouchableHighlight, Image } from "react-native";
import { Text, Label } from "../../ui";
import style from "./profileOrdersCountStyle";

const ProfileInfo = props => {
  const { locale, data } = props;
  const { currentOrders = 0, completedOrders = 0, allOrders = 0 } = data;
  return (
    <View style={style.container}>
      <View style={[style.orderContainer, style.orderSeparator]}>
        <Text
          level={"huge"}
          weight={"bold"}
          density={"loose"}
          style={style.orderCount}
        >
          {currentOrders}
        </Text>
        <Text level={"large"} style={style.orderLabel}>
          {locale.profile.currentOrders}
        </Text>
      </View>

      <View style={[style.orderContainer, style.orderSeparator]}>
        <Text
          level={"huge"}
          weight={"bold"}
          density={"loose"}
          style={style.orderCount}
        >
          {completedOrders}
        </Text>
        <Text level={"large"} style={style.orderLabel}>
          {locale.profile.completedOrders}
        </Text>
      </View>

      <View style={style.orderContainer}>
        <Text
          level={"huge"}
          weight={"bold"}
          density={"loose"}
          style={style.orderCount}
        >
          {allOrders}
        </Text>
        <Text level={"large"} style={style.orderLabel}>
          {locale.profile.allOrders}
        </Text>
      </View>
    </View>
  );
};
export default ProfileInfo;
