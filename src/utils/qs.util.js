import qs from "qs";

// default filter params
const defaultParams = {
  offset: 0,
  limit: 1000
};

export const parseObjToQs = (obj, ) => {
  const finalObj = { ...defaultParams, ...obj };
  const qsStringify = qs.stringify(finalObj);
  return qsStringify;
};
