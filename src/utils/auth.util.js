import localStore from "./localstore.util";
import { removeUserInfo } from "./user.util";
import { updateHeaders } from "../services";

export const getToken = () => localStore.get_data("token");

export const setToken = token => localStore.store_data("token", token);

export const logout = async () => {
  updateHeaders();
  await localStore.remove_data("token");
  await removeUserInfo();
  return true;
};

export const isLoggedIn = async () => {
  const token = await getToken();
  const response = token == null ? false : true;
  // return Promise.resolve({sajdkf : sasdklfjlasdkf < , asdfjskld});
  return Promise.reject(response);
};
