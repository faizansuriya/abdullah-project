import EventEmitter from "EventEmitter";
import { PermissionsAndroid, Alert, AsyncStorage } from "react-native";
import Permissions from "react-native-permissions";
import openSettings from "react-native-open-settings";
import { reset } from "ansi-colors";

let emitter = new EventEmitter();
let watchId;

const listenerCallbacks = [];

const EMITTER_KEY = "TWIM Location";

let lastPosition = null;

export function getCurrentPosition() {
  return lastPosition;
}

export const checkAndStartListening = async () => {
  Permissions.request("location")
    .then(response => {
      if (response === "authorized") {
        startListening();
      } else if (response === "denied") {
        Alert.alert(
          "Oops",
          "You haven't allowed to use your location. Go to Settings to modify this 😉",
          [
            {
              text: "Not Now"
            },
            {
              text: "Settings",
              onPress: () => openSettings.openSettings(),
              style: "cancel"
            }
          ]
        );
      }
    })
    .catch(error => {
      console.log("error inside permissions", error);
    });

  // });
};

function startListening() {
  navigator.geolocation.getCurrentPosition(
    position => {
      lastPosition = position;
      emitter.emit(EMITTER_KEY, position);
    },
    error => {
      console.log("error location not found", error);
    }
  );

  /*navigator.geolocation.getCurrentPosition(position => {
    emitter.emit(EMITTER_KEY, position);
  });*/

  watchId = navigator.geolocation.watchPosition(
    position => {
      lastPosition = position;
      emitter.emit(EMITTER_KEY, position);
    },
    error => {
      Alert.alert(
        "تنبيه",
        "لم تسمح لك باستخدام موقعك. انتقل إلى الإعدادات لتعديل هذا"
      );
    },
    {
      // enableHighAccuracy: true,
      timeout: 20000,
      maximumAge: 1000
      // distanceFilter: 5
    }
  );
}

function stopListening() {
  navigator.geolocation.clearWatch(this.watchId);
  emitter.removeAllListeners(EMITTER_KEY);
  delete this.watchId;
}

export function addLocationListener(callback) {
  emitter.addListener(EMITTER_KEY, callback);
  listenerCallbacks.push(callback);

  if (lastPosition) {
    callback(lastPosition);
  }

  if (listenerCallbacks.length > 0 && !this.watchId) {
    checkAndStartListening();
  }
}

export function removeLocationListener(callback) {
  emitter.removeListener(EMITTER_KEY, callback);
  const index = listenerCallbacks.indexOf(callback);
  listenerCallbacks.splice(index, 1);

  if (listenerCallbacks.length === 0) {
    stopListening();
  }
}
