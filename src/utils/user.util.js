import localStore from "./localstore.util";

export const setUserInfo = async info => {
  const _data = { ...info };
  await localStore.store_data("userinfo", _data);
  return true;
};
export const getUserInfo = () => localStore.get_data("userinfo");

export const removeUserInfo = () => localStore.remove_data("userinfo");
