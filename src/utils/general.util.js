import locale from "../resources/strings/ar";
import _ from "lodash";
export function parseNumber(number) {
  return number.toString().replace(/[0123456789]/g, s => locale.numbers[s]);
}

export function requestParseError(error) {
  const msg = _.get(error, "response.data.message");
  return [
    "ERROR_INVALID_TOKEN",
    "ERROR_NO_TOKEN",
    "ERROR_AUTH_FAILED",
    "ERROR_INVALID_TOKEN"
  ].includes(msg)
    ? "Please log in"
    : msg;
}
