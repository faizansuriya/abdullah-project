import React from "react";

import { StackNavigator } from "react-navigation";

import SettingScreen from "../screens/SettingScreen";
import PrivacyPolicyScreen from "../screens/PrivacyPolicyScreen";
import AboutTheAppScreen from "../screens/AboutTheAppScreen";
import BankAccountsScreen from "../screens/BankAccountsScreen";
import ContactUsScreen from "../screens/ContactUsScreen";
import DevelopedByScreen from "../screens/DevelopedByScreen";

import { navigationStyle } from "../styles/generalStyle";
import backButtonImage from "../resources/assets/icons/back_arrow.png";
import locale from "../resources/strings/ar";

export default StackNavigator(
  {
    Setting: {
      screen: SettingScreen,
      navigationOptions: {
        title: locale.settings.title
      }
    },
    PrivacyPolicy: {
      screen: PrivacyPolicyScreen,
      navigationOptions: {
        title: locale.settings.privacyPolicy
      }
    },
    AboutTheApp: {
      screen: AboutTheAppScreen,
      navigationOptions: {
        title: locale.settings.aboutTheApp
      }
    },
    BankAccounts: {
      screen: BankAccountsScreen,
      navigationOptions: {
        title: locale.settings.bankAccounts
      }
    },
    ContactUs: {
      screen: ContactUsScreen,
      navigationOptions: {
        title: locale.settings.contactUs
      }
    },
    DevelopedBy: {
      screen: DevelopedByScreen,
      navigationOptions: {
        title: locale.settings.developedBy
      }
    }
  },
  {
    initialRouteName: "Setting",
    navigationOptions: () => {
      return {
        ...navigationStyle,
        headerBackTitle: null,
        headerBackImage: backButtonImage,
        locale: locale
      };
    }
  }
);
