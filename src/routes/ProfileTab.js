import React, { Component } from "react";

import { StackNavigator, withNavigationFocus } from "react-navigation";

import ProfileScreen from "../screens/ProfileScreen";
import ProfileEditScreen from "../screens/ProfileEditScreen";

import { navigationStyle } from "../styles/generalStyle";
import backButtonImage from "../resources/assets/icons/back_arrow.png";
import locale from "../resources/strings/ar";
import _ from "lodash";
import LoginCheckHOC from "../components/LoginCheckHOC";
const ProfileStackNav = StackNavigator(
  {
    Profile: {
      screen: ProfileScreen,
      navigationOptions: ({ navigation }) => {
        return { title: locale.profile.title };
      }
    },
    ProfileEdit: {
      screen: ProfileEditScreen,
      navigationOptions: ({ navigation }) => {
        return { title: locale.editProfile.title };
      }
    }
  },
  {
    navigationOptions: () => {
      return {
        ...navigationStyle,
        headerBackTitle: null,
        headerBackImage: backButtonImage,
        locale: locale
      };
    }
  }
);

class UserProfileScreen extends Component {
  render() {
    return (
      <LoginCheckHOC {..._.pick(this.props, ["navigation", "isFocused"])}>
        <ProfileStackNav />
      </LoginCheckHOC>
    );
  }
}
export default withNavigationFocus(UserProfileScreen);
