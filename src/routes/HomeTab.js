import React from "react";

import { StackNavigator } from "react-navigation";

import SplashScreen from "../screens/SplashScreen";
import LoginScreen from "../screens/LoginScreen";
import MoveToLoginWithResetScreen from "../screens/MoveToLoginWithResetScreen";

import ForgotPasswordScreen from "../screens/ForgotPasswordScreen";
import RegistrationScreen from "../screens/RegistrationScreen";
import MobileConfirmationScreen from "../screens/MobileConfirmationScreen";
import ServicesScreen from "../screens/ServicesScreen";
import PlaceOrderScreen from "../screens/PlaceOrderScreen";

import { navigationStyle } from "../styles/generalStyle";
import backButtonImage from "../resources/assets/icons/back_arrow.png";
import locale from "../resources/strings/ar";

export default StackNavigator(
  {
    Login: {
      screen: LoginScreen,
      navigationOptions: ({ navigation }) => {
        return { title: locale.login.title };
      }
    },
    LoginWithReset: {
      screen: MoveToLoginWithResetScreen,
      navigationOptions: ({ navigation }) => {
        return { title: null, header: null };
      }
    },
    ForgotPassword: {
      screen: ForgotPasswordScreen,
      navigationOptions: {
        title: locale.forgotPassword.title
      }
    },

    Registration: {
      screen: RegistrationScreen,
      navigationOptions: {
        title: locale.registration.title
      }
    },
    MobileConfirmation: {
      screen: MobileConfirmationScreen,
      navigationOptions: {
        title: locale.mobileConfirmation.title
      }
    },

    Services: {
      screen: ServicesScreen,
      navigationOptions: {
        title: locale.services.title
      }
    },

    PlaceOrder: {
      screen: PlaceOrderScreen,
      navigationOptions: {
        title: locale.order.title
      }
    }
  },
  {
    initialRouteName: "Login",
    navigationOptions: () => {
      return {
        ...navigationStyle,
        headerBackTitle: null,
        headerBackImage: backButtonImage,
        locale: locale
      };
    }
  }
);
