import React, { Component } from "react";

import { Image } from "react-native";
import { TabNavigator, TabBarBottom } from "react-navigation";
import { navigationStyle, tabBarStyle } from "../styles/generalStyle";

import HomeStack from "./HomeTab";
import SettingStack from "./SettingTab";
import ProfileStack from "./ProfileTab";
import OrderListStack from "./OrderListTab";

const _iconLocation = "../resources/assets/icons/footer/";
const HomeIcon = require(`${_iconLocation}home.png`);
const HomeIconActive = require(`${_iconLocation}home_active.png`);
const ProfileIcon = require(`${_iconLocation}account.png`);
const ProfileIconActive = require(`${_iconLocation}account_active.png`);
const OrderListIcon = require(`${_iconLocation}orders.png`);
const OrderListIconActive = require(`${_iconLocation}orders_active.png`);
const SettingsIconActive = require(`${_iconLocation}settings_active.png`);
const SettingsIcon = require(`${_iconLocation}settings.png`);

const _tabBarIconsConfig = {
  HomeIconActive,
  HomeIcon,
  ProfileIconActive,
  ProfileIcon,
  OrderListIconActive,
  OrderListIcon,
  SettingsIconActive,
  SettingsIcon
};
export default TabNavigator(
  {
    Home: { screen: HomeStack },
    Profile: { screen: ProfileStack },
    Settings: { screen: SettingStack },
    OrderList: { screen: OrderListStack }
  },
  {
    initialRouteName: "Home",
    order: ["Settings", "Profile", "OrderList", "Home"],
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        const _iconName = _tabBarIconsConfig[`${routeName}Icon`];
        const _iconNameFocused = _tabBarIconsConfig[`${routeName}IconActive`];
        const _source = focused ? _iconNameFocused : _iconName;
        const _style = { height: 22, width: 22 };
        return <Image source={_source} style={_style} />;
      },
      title: ""
    }),
    ...tabBarStyle,
    tabBarComponent: TabBarBottom,
    tabBarPosition: "bottom",
    animationEnabled: false,
    swipeEnabled: false
  }
);
