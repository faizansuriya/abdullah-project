import React from "react";

import { StackNavigator } from "react-navigation";

import OrderListScreen from "../screens/OrderListScreen";

import { navigationStyle } from "../styles/generalStyle";
import backButtonImage from "../resources/assets/icons/back_arrow.png";
import locale from "../resources/strings/ar";

export default StackNavigator(
  {
    OrderList: {
      screen: OrderListScreen,
      navigationOptions: ({ navigation }) => {
        return { title: locale.orderList.title };
      }
    }
  },
  {
    navigationOptions: () => {
      return {
        ...navigationStyle,
        headerBackTitle: null,
        headerBackImage: backButtonImage,
        locale: locale
      };
    }
  }
);
