import { StyleSheet } from "react-native";
import Colors from "../resources/colors";

const labelColor = "#4e4d50";
const inactiveColor = "#dedbe3";

export const navigationStyle = {
  headerStyle: {
    backgroundColor: "#F1BA45",
    elevation: 0
  },
  headerTintColor: "white",
  headerTitleStyle: {
    fontFamily: "NeoSansW23-Medium",
    fontWeight: "bold",
    flex: 1,
    color: "rgba(30,28,26, .7)",
    textAlign: "center"
  }
};

export const tabBarStyle = {
  tabBarOptions: {
    activeTintColor: "#686868",
    inactiveTintColor: "#cfcdd2"
  }
};

export const labelStyle = StyleSheet.create({
  style: {
    color: labelColor,
    textAlign: "right"
  }
});

export const inputStyle = StyleSheet.create({
  container: {
    borderColor: inactiveColor,
    borderBottomWidth: 2
  },
  style: {
    height: 25,
    color: labelColor,
    textAlign: "right",
    padding: 0,
    margin: 0,
    fontFamily: "Neo Sans Arabic"
  }
});

export const container = StyleSheet.create({
  style: {
    display: "flex",
    flexDirection: "row",
    flex: 1,
    padding: 5,
    backgroundColor: "white"
  }
});

export const content = StyleSheet.create({
  style: {
    display: "flex",
    flex: 1,
    flexDirection: "column"
  }
});

export const form = StyleSheet.create({
  style: {
    display: "flex",
    flexDirection: "column"
  }
});

export const alignItemEnd = StyleSheet.create({
  style: {
    alignItems: "flex-end"
  }
});

export const textCenter = StyleSheet.create({
  style: {
    textAlign: "center"
  }
});
export const underline = StyleSheet.create({
  style: {
    textDecorationLine: "underline"
  }
});
export const itemCenter = StyleSheet.create({
  style: { alignItems: "center" }
});

export const controlMargin = StyleSheet.create({
  style: { margin: 5 }
});
