/* order apis */
import _ from "lodash";
import { get, post, put } from "./base.service";

const SERVICE_URLS = _.mapValues(
  {
    createOrder: "/orders",
    getOrders: "/orders?${qs}",
    updateOrder: "/orders/${id}",
    getCounts: "/orders/count"
  },
  _.template
);

const createOrder = payload => post(SERVICE_URLS.createOrder(), payload);
const getOrders = qs => get(SERVICE_URLS.getOrders({ qs }));
const updateOrder = payload =>
  put(SERVICE_URLS.updateOrder({ id: payload.id }), payload);
const getCounts = () => get(SERVICE_URLS.getCounts());

export default {
  createOrder,
  getOrders,
  updateOrder,
  getCounts
};
