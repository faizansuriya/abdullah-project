/* review apis */
import _ from "lodash";
import { get, post } from "./base.service";

const SERVICE_URLS = _.mapValues(
  {
    getReviews: "/orders/all/reviews",
    create: "/orders/${orderId}/reviews"
  },
  _.template
);

const getReviews = params => get(SERVICE_URLS.getReviews());
const create = payload =>
  post(SERVICE_URLS.create({ orderId: payload.orderId }), payload);

export default {
  getReviews,
  create
};
