import axios from "axios";
import { Config } from "../utils/config.util";
import { getToken } from "../utils/auth.util";

// const BASE_URL = "http://192.168.8.100:4001";
const BASE_URL = Config.API_URL;

function log() {
  if (__DEV__) console.log.apply(console, arguments);
}

export function GetApiRequestHeader(token) {
  const authToken = token || getToken();
  return {
    Accept: "application/json",
    "Content-Type": "application/json",
    "x-access-token": authToken,
    Authorization: `Bearer ${authToken}`
  };
}

const instance = axios.create({
  baseURL: `${BASE_URL}/api`,
  timeout: 60000,
  withCredentials: true
  // headers: GetApiRequestHeader()
});

export function updateHeaders(token = "") {
  // Alter defaults after instance has been created
  const header = GetApiRequestHeader(token);
  instance.defaults.headers = header;
}

export async function request({ method, url, data, headers }) {
  log(`Sending ${method} request to ${BASE_URL}`, url);
  var promise = instance[method](url, data);
  const response = await promise;

  log(`Response from ${url}`, response);
  const payload = response.data;

  if (headers) {
    return {
      data: payload,
      headers: response.headers
    };
  }

  return payload;
}

export async function get(url, params, config) {
  return request({ method: "get", url, data: { params }, ...config });
}

export async function del(url, params, config) {
  return request({ method: "delete", url, data: { params }, ...config });
}

export async function post(url, data, config) {
  return request({ method: "post", url, data, ...config });
}

export async function put(url, data, config) {
  return request({ method: "put", url, data, ...config });
}
