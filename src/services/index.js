import { updateHeaders } from "./base.service";

// system module services
import authService from "./auth.service";
import userService from "./user.service";
import orderService from "./order.service";
import reviewService from "./review.service";

export default {
  user: userService,
  auth: authService,
  order: orderService,
  review: reviewService
};

export { updateHeaders };
