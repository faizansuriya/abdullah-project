/* user apis */
import _ from "lodash";
import { get, post, put } from "./base.service";

const SERVICE_URLS = _.mapValues(
  {
    getUsers: "/users",
    register: "/users",
    mobileConfirmation: "/users/${id}/mobile_confirmation/${code}",
    update: "/users/${id}",
    forgotPassword: "/users/forgot_password"
  },
  _.template
);

const getUsers = params => get(SERVICE_URLS.getUsers());
const register = payload => post(SERVICE_URLS.register(), payload);
const mobileConfirmation = payload =>
  get(SERVICE_URLS.mobileConfirmation(payload));
const forgotPassword = payload => post(SERVICE_URLS.forgotPassword(), payload);
const update = payload => put(SERVICE_URLS.update({ id: "me" }), payload);

export default {
  getUsers,
  register,
  update,
  mobileConfirmation,
  forgotPassword
};
