/* auth apis */
import _ from "lodash";

import { get, post } from "./base.service";

const SERVICE_URLS = _.mapValues(
  {
    login: "/auth/login",
    logout: "/logout/",
    getProfile: "/auth/auth-checker"
  },
  _.template
);

const login = ({ email, password }) =>
  post(SERVICE_URLS.login(), { email, password });

export const logout = () => post(SERVICE_URLS.logout());

export const whoAmI = () => get(SERVICE_URLS.getProfile());

const authService = {
  login,
  logout,
  whoAmI
};

export default authService;
