import { StyleSheet } from "react-native";
import { Image } from "react-native";

export default StyleSheet.create({
  settingsContainer: {
    flex: 1,
    flexDirection: "column"
  },
  settingBlock: {},
  settingButton: {
    padding: 20,
    borderBottomWidth: 1,
    borderColor: "lightgrey",
    justifyContent: "center"
  },

  settingBlockInner: {
    alignItems: "center",
    flexDirection: "row"
  },
  separator: {
    height: 16,
    width: 16,
    alignItems: "center",
    justifyContent: "center",

    margin: 5
  },
  settingImage: {
    height: 20,
    width: 20,
    alignItems: "center",
    justifyContent: "center",
    resizeMode: Image.resizeMode.contain,
    margin: 5
  },
  settingName: {
    alignItems: "flex-start",
    textAlign: "right",
    justifyContent: "center",
    color: "black",
    margin: 5,
    flex: 1
  },
  enterArrowImage: {
    height: 20,
    width: 20,
    alignItems: "center",
    justifyContent: "center",
    resizeMode: Image.resizeMode.contain,
    margin: 5
  }
});
