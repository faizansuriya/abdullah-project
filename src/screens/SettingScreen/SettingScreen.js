import React, { Component } from "react";
import {
  Alert,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Share,
  Platform
} from "react-native";
import { NavigationActions, withNavigationFocus } from "react-navigation";

import { Text, Button, TextInput, Label } from "../../components/ui";
import styles from "./settingScreenStyle.js";
import Rate, { AndroidMarket } from "react-native-rate";
import * as generalStyles from "../../styles/generalStyle";
import locale from "../../resources/strings/ar";
import * as authUtil from "../../utils/auth.util";
import LoginCheckHOC from "../../components/LoginCheckHOC";
import _ from "lodash";
const iconLocation = "../../resources/assets/icons/settings";
const enterArrowImage = require(`${iconLocation}/enter.png`);

const loginSettingOpt = {
  type: "login",
  name: locale.settings.login,
  icon: require(`${iconLocation}/logout.png`),
  screenName: "LoginWithReset"
};
const settingsList = [
  {
    type: "privacyPolicy",
    name: locale.settings.privacyPolicy,
    icon: require(`${iconLocation}/privacyPolicy.png`),
    screenName: "PrivacyPolicy"
  },
  {
    type: "aboutTheApp",
    name: locale.settings.aboutTheApp,
    icon: require(`${iconLocation}/aboutTheApp.png`),
    screenName: "AboutTheApp"
  },
  {
    type: "bankAccounts",
    name: locale.settings.bankAccounts,
    icon: require(`${iconLocation}/bankAccounts.png`),
    screenName: "BankAccounts"
  },
  {
    type: "contactUs",
    name: locale.settings.contactUs,
    icon: require(`${iconLocation}/contactUs.png`),
    screenName: "ContactUs"
  },
  {
    type: "shareTheApp",
    name: locale.settings.shareTheApp,
    icon: require(`${iconLocation}/shareTheApp.png`)
  },
  {
    type: "rateTheApp",
    name: locale.settings.rateTheApp,
    icon: require(`${iconLocation}/rateTheApp.png`)
  },
  {
    type: "developedBy",
    name: locale.settings.developedBy,
    icon: require(`${iconLocation}/developedBy.png`),
    screenName: "DevelopedBy"
  },
  {
    type: "separator"
  },
  {
    type: "logout",
    name: locale.settings.logout,
    icon: require(`${iconLocation}/logout.png`)
  }
];

function GeneralRowComp({ setting, onClick }) {
  return (
    <View style={styles.settingBlock}>
      <TouchableOpacity style={styles.settingButton} onPress={onClick}>
        <View style={styles.settingBlockInner}>
          <Image source={enterArrowImage} style={styles.enterArrowImage} />
          <Text level={"extra_large"} style={styles.settingName}>
            {setting.name}
          </Text>
          <Image source={setting.icon} style={styles.settingImage} />
        </View>
      </TouchableOpacity>
    </View>
  );
}
class SettingScreen extends Component {
  _logoutClick = async () => {
    await authUtil.logout();
    this.setState({ test: "test" });
  };
  _handleClick = params => {
    const { type, screenName } = params;
    if (this[`_${type}Click`]) {
      this[`_${type}Click`]();
      return;
    }
    if (screenName) {
      this.props.navigation.navigate(screenName);
      return;
    }
    Alert.alert("تنبيه", `Selected type is ${type}`);
  };

  _rateTheAppClick = () => {
    let options = {
      AppleAppID: "1397122250",
      GooglePackageName: "com.facebook.app",
      AmazonPackageName: "com.facebook.app",
      OtherAndroidURL: "http://www.facebook.com",
      preferredAndroidMarket: AndroidMarket.Google,
      preferInApp: false,
      fallbackPlatformURL: "http://www.facebook.com"
    };
    Rate.rate(options, success => {
      if (success) {
        // this technically only tells us if the user successfully went to the Review Page. Whether they actually did anything, we do not know.
        this.setState({ rated: true });
      }
    });
  };

  _shareTheAppClick = () => {
    const message = ` ${this._getAppUrl()}`;
    Share.share({ title: null, message });
  };
  _getAppUrl() {
    if (Platform.OS == "android") {
      return "حمل تطبيق المساند الأفضل الآن https://rebrand.ly/vxf1454";
    } else {
      return "حمل تطبيق المساند الأفضل الآن https://rebrand.ly/c56d26";
    }
  }

  _renderRow = (setting, i) => {
    if (setting.type === "separator")
      return (
        <View key={i} style={styles.settingBlock}>
          <TouchableOpacity style={styles.settingButton}>
            <View style={[styles.settingBlockInner, styles.separator]} />
          </TouchableOpacity>
        </View>
      );

    if (setting.type === "logout") {
      return (
        <LoginCheckHOC
          {..._.pick(this.props, ["navigation", "isFocused"])}
          key={i}
          LoginComp={
            <GeneralRowComp
              key={"log"}
              {...{ setting: loginSettingOpt }}
              onClick={this._handleClick.bind(this, loginSettingOpt)}
            />
          }
        >
          <GeneralRowComp
            {...{ setting }}
            onClick={this._handleClick.bind(this, setting)}
          />
        </LoginCheckHOC>
      );
    }

    return (
      <GeneralRowComp
        key={i}
        {...{ setting }}
        onClick={this._handleClick.bind(this, setting)}
      />
    );
  };
  render() {
    const { isLoading } = this.props;

    return (
      <ScrollView
        style={{ flex: 1, backgroundColor: "white" }}
        contentStyle={generalStyles.container.style}
        contentContainerStyle={{ backgroundColor: "white", marginBottom: 10 }}
      >
        <View padder style={generalStyles.content.style}>
          <View style={styles.settingsContainer}>
            {settingsList.map(this._renderRow)}
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default withNavigationFocus(SettingScreen);
