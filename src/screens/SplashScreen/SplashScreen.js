import React from "react";
import { View, Image } from "react-native";
import { Text, Button } from "../../components/ui";
import styles from "./splashScreenStyle.js";

const _centerImage = require("../../resources/assets/splash.png");
const _bottomImage = require("../../resources/assets/splash_bottom.png");

class SplashScreen extends React.Component {
  _goToLogin = () => {
    this.props.navigation.navigate("Login");
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.centerImageContainer}>
          <Image source={_centerImage} style={styles.centerImage} />
        </View>
        <View style={styles.bottomImageContainer}>
          <Image source={_bottomImage} style={styles.bottomImage} />
        </View>
      </View>
    );
  }
}

export default SplashScreen;
