import { StyleSheet } from "react-native";
import { Image } from "react-native";
export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white"
  },

  centerImageContainer: {
    flex: 2,
    justifyContent: "flex-end",
    alignItems: "center",
    paddingLeft: 20,
    paddingRight: 20
  },

  centerImage: {
    width: "100%",
    height: "100%",
    resizeMode: Image.resizeMode.contain
  },
  bottomImageContainer: {
    flex: 2,
    justifyContent: "flex-end"
  },
  bottomImage: {
    width: "100%",
    height: "100%"
  }
});
