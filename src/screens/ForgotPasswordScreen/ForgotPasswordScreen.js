import React, { Component } from "react";
import { Switch, View, TouchableHighlight, Alert } from "react-native";

import styles from "./ForgotPasswordScreenStyle.js";
import * as generalStyles from "../../styles/generalStyle";
import locale from "../../resources/strings/ar";
import { Text, Button, TextInput, Label } from "../../components/ui";
import _ from "lodash";
import { requestParseError } from "../../utils/general.util";
import services from "../../services";

class MobileConfirmationScreen extends Component {
  state = {
    email: ""
  };

  _handleOnChange = (key, val) => {
    this.setState(() => ({ [key]: val }));
  };

  _handleSubmit = async () => {
    const payload = { ...this.state };
    try {
      const response = await services.user.forgotPassword(payload);
      Alert.alert("تنبيه", locale.forgotPassword.successMessage);
      this.props.navigation.navigate("Login");
    } catch (ex) {
      const _msg = requestParseError(ex);
      Alert.alert("تنبيه", _msg);
    }
  };

  render() {
    const { email } = this.state;

    return (
      <View style={generalStyles.container.style}>
        <View padder style={generalStyles.content.style}>
          <View padder style={generalStyles.form.style}>
            <View regular style={generalStyles.alignItemEnd.style}>
              <Label style={{marginTop : 20, paddingRight: 10}} level="large">
                {locale.forgotPassword.enterEmailText}
              </Label>
            </View>
            <View style={generalStyles.form.style}>
              <View regular style={[generalStyles.controlMargin.style]}>
                <Label style={[styles.labelStyle,{marginTop : 10}] } level="large">
                  {locale.forgotPassword.email}
                </Label>
                <TextInput
                containerStyle={styles.textInputStyle}
                  value={email}
                  onChangeText={this._handleOnChange.bind(this, "email")}
                />
              </View>
            </View>

            <Button
              containerStyle={styles.btn}
              block
              onPress={this._handleSubmit}
              label={locale.forgotPassword.recover}
              labelLevel="huge"
              labelWeight="bold"
            />
          </View>
        </View>
      </View>
    );
  }
}

export default MobileConfirmationScreen;
