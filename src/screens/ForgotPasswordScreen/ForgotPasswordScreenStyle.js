import { StyleSheet } from "react-native";

const linkColor = "#2b0fda";
const inactiveColor = "#dedbe3";
export default StyleSheet.create({
  tncLinkText: {
    textDecorationLine: "underline",
    color: linkColor
  },
  labelStyle: { marginBottom: 5 , marginRight : 5 },

  btn : {
    marginTop : 20,
    marginBottom: 20,
    backgroundColor: "black",
    borderWidth: 1,
    borderColor: "#ebebeb",
    paddingVertical: 13,
    marginHorizontal: 5,
    marginRight : 5,
  },
  textInputStyle : {
    borderColor: inactiveColor,
    marginRight : 5,
    marginTop : 20,
    marginHorizontal: 3,
    borderBottomWidth: 2
  }
});
