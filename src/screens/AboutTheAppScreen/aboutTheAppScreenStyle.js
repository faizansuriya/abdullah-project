import { StyleSheet } from "react-native";
import { Image } from "react-native";

export default StyleSheet.create({
  ataContainer: {
    flex: 1,
    flexDirection: "column",
    padding: 5,
    justifyContent: "flex-start"
  },
  ataRow: {
    margin: 10,
    color: "black",
    textAlign: "right",
    justifyContent: "flex-start"
  },
  appNameImgContainer: {
    alignItems: "center",
    paddingLeft: 10,
    margin: 10,
    justifyContent: "flex-start"
  },

  appNameImg: {
    width: 250,
    height: 250,
    resizeMode: Image.resizeMode.contain
  }
});
