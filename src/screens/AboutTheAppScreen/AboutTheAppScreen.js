import React, { Component } from "react";
import { Switch, View, TouchableOpacity, Image } from "react-native";

import styles from "./aboutTheAppScreenStyle.js";
import * as generalStyles from "../../styles/generalStyle";
import locale from "../../resources/strings/ar";
import { Text, Button, TextInput, Label } from "../../components/ui";
const _appImage = require("../../resources/assets/splash.png");

const ataPoints = [
  `تعد أعمال الصيانة من أهم الأعمال التي نحتاج إليها بشكل دوري كأفراد او كمؤسسات حيث غالبا ما نحتاج الى عمل صيانة للسباكة المنزلية او شبكة الكهرباء وغيرها من الخدمات، لكن يصعب علينا العثور على فني متخصص بالسعر المناسب ، هنا يأتي دور تطبيق “المساند الأفضل” حيث نقدم لكم خدمات إحترافية بجودة عالية.`,
  "“المساند الأفضل” هو تطبيق عصري لتسهيل الحصول على خدمات الصيانة المنزلية و أعمال البناء في دقائق قليلة. نحرص على تزويد عملاءنا بأفضل الأسعار وأجود الخدمات من خلال فريق عمل مميز يخدمكم على مدار الساعة."
];

class AboutTheAppScreen extends Component {
  render() {
    const { isLoading } = this.props;

    return (
      <View style={generalStyles.container.style}>
        <View padder style={generalStyles.content.style}>
          <View style={styles.ataContainer}>
            <View style={styles.appNameImgContainer}>
              <Image source={_appImage} style={styles.appNameImg} />
            </View>
            {ataPoints.map((row, i) => (
              <Text
                weight={"bold"}
                level={"large"}
                style={styles.ataRow}
                key={i}
              >
                {row}
              </Text>
            ))}
          </View>
        </View>
      </View>
    );
  }
}

export default AboutTheAppScreen;
