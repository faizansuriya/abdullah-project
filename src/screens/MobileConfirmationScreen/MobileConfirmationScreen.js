import React, { Component } from "react";
import { Switch, View, TouchableHighlight, Alert } from "react-native";

import styles from "./mobileConfirmationScreenStyle.js";
import * as generalStyles from "../../styles/generalStyle";
import locale from "../../resources/strings/ar";
import { Text, Button, TextInput, Label } from "../../components/ui";
import _ from "lodash";
import services from "../../services";
import { parseNumber, requestParseError } from "../../utils/general.util";
import CodeInput from "react-native-code-input";
import Colors from "../../resources/colors.js";
/*const CodeInput = ({ onChange, data, focusedKey }) => {
  const _tiStyle = {
    padding: 10,
    width: 30
  };
  return _.times(4, i => {
    const isFocused = focusedKey === i;
    return (
      <TextInput
        autoFocus={isFocused}
        keyboardType={"none"}
        style={[generalStyles.textCenter.style, _tiStyle]}
        key={`${i}-${focusedKey}`}
        value={data[i] || ""}
        keyboardType="numeric"
        maxLength={1}
        onChangeText={val => {
          onChange(i, val.replace(/[^0-9]/g, ""));
        }}
      />
    );
  });
};*/
class MobileConfirmationScreen extends Component {
  state = {
    code: "",
    focusedKey: 0
  };

  _handleOnChange = (key, val) => {
    this.setState(() => ({ [key]: val }));
  };

  _handleOnCodeChange = code => {
    this.setState(
      () => ({
        code
      }),
      () => {}
    );
  };

  _handleSubmit = async () => {
    const code = this.state.code;
    const user = _.get(this.props, "navigation.state.params.user", {});

    const payload = { code, id: user.id };

    if (this.refs.arr.state.currentIndex !== 4) {
      Alert.alert("تنبيه", "Please fill the code input completely.");
    } else {
      try {
        const response = await services.user.mobileConfirmation(payload);
        Alert.alert("تنبيه", locale.mobileConfirmation.successMessage);
        this.props.navigation.navigate("LoginWithReset");
      } catch (ex) {
        const _msg = "يرجى تقديم رمز صالح";
        Alert.alert("تنبيه", _msg);
        this.setState({ code: 0 });
      }
    }
  };

  render() {
    const user = _.get(this.props, "navigation.state.params.user", {});
    const { isLoading } = this.props;
    const { code } = this.state;
    const mobile = user.mobile || "1234567890";
    return (
      <View style={generalStyles.container.style}>
        <View padder style={generalStyles.content.style}>
          <View padder style={generalStyles.form.style}>
            <View regular style={generalStyles.alignItemEnd.style}>
              <Label style={styles.labelStyle} level="large">
                {locale.mobileConfirmation.enterCodeText}
              </Label>
              <Label style={styles.labelStyle} level="large">
                {parseNumber(mobile)}
              </Label>
            </View>

            <View
              regular
              style={[
                generalStyles.alignItemEnd.style,
                { alignItems: "center" }
              ]}
            >
              <View
                style={{
                  width: 200,
                  flexDirection: "row-reverse",
                  justifyContent: "space-between",
                  marginBottom: 30
                }}
              >
                {/* <CodeInput
                  onChange={this._handleOnCodeChange}
                  data={code}
                  focusedKey={focusedKey}
              />*/}

                <CodeInput
                  activeColor={Colors.LightGrey}
                  inactiveColor={Colors.LightGrey}
                  inputPosition="center"
                  keyboardType="numeric"
                  size={30}
                  onFulfill={(isValid, code) =>
                    this._handleOnCodeChange(isValid, code)
                  }
                  containerStyle={{ marginTop: 20, marginBottom: 20 }}
                  codeInputStyle={{
                    borderBottomWidth: 1.5,
                    color: Colors.Black
                  }}
                  ref={"arr"}
                  codeLength={4}
                  autoFocus={true}
                  compareWithCode={() => {}} //	code to compare. if null, onFulfill callback return inputted code to check later
                />
              </View>
            </View>

            <Button
              containerStyle={styles.btn}
              block
              underlayColor={Colors.Black}
              onPress={this._handleSubmit}
              label={locale.mobileConfirmation.register}
              labelWeight={"bold"}
              labelLevel={"huge"}
            />
          </View>
        </View>
      </View>
    );
  }
}

export default MobileConfirmationScreen;
