import { StyleSheet } from "react-native";
import Colors from "../../resources/colors";

const linkColor = "#2b0fda";
export default StyleSheet.create({
  tncLinkText: {
    textDecorationLine: "underline",
    color: linkColor
  },
  labelStyle: {
    marginTop: 10,
    marginBottom: 5,
    marginRight: 5,
    textAlign: "right"
  },
  
  btn: {
    backgroundColor: Colors.Black,
    borderWidth: 1,
    borderColor: "#ebebeb",
    paddingVertical: 13
  },
});
