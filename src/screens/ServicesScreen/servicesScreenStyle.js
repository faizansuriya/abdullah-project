import { StyleSheet } from "react-native";
import { Image } from "react-native";

export default StyleSheet.create({
  servicesContainer: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap"
  },
  serviceBlock: {
    width: "50%",
    padding: 5
  },
  serviceButton: {},

  serviceBlockInner: {
    padding: 10,
    margin: 10,
    alignItems: "center",
    elevation: 2,
    backgroundColor: "white",
    borderRadius: 10,
    height: 100,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.4,
    shadowRadius: 1
  },
  serviceImage: {
    marginTop: 15,
    height: 50,
    width: 50,
    alignItems: "center",
    justifyContent: "center",
    resizeMode: Image.resizeMode.contain
  },
  serviceName: {
    alignItems: "center",
    marginTop: 10,
    alignSelf: "center"
  }
});
