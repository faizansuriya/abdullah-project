import React, { Component } from "react";
import { View, TouchableOpacity, Image } from "react-native";

import styles from "./servicesScreenStyle.js";
import * as generalStyles from "../../styles/generalStyle";
import locale from "../../resources/strings/ar";
import { Text, Button, TextInput, Label } from "../../components/ui";
import * as authUtil from "../../utils/auth.util";
import { NavigationActions } from "react-navigation";
const iconLocation = "../../resources/assets/icons/services";
const servicesList = [
  {
    type: "electricity",
    name: locale.services.electricity,
    icon: require(`${iconLocation}/electricity.png`),
    order: 1
  },
  {
    type: "carpentry",
    name: locale.services.carpentry,
    icon: require(`${iconLocation}/carpentry.png`),
    order: 3
  },
  {
    type: "refrigeration",
    name: locale.services.refrigeration,
    icon: require(`${iconLocation}/refrigeration.png`),
    order: 4
  },
  {
    type: "plumbing",
    name: locale.services.plumbing,
    icon: require(`${iconLocation}/plumbing.png`),
    order: 2
  },
  {
    type: "paint",
    name: locale.services.paint,
    icon: require(`${iconLocation}/paint.png`),
    order: 6
  },
  {
    type: "cleaning",
    name: locale.services.cleaning,
    icon: require(`${iconLocation}/cleaning.png`),
    order: 5
  }
].sort((a, b) => a.order - b.order);
class ServicesScreen extends Component {
  _handleClick = async type => {
    const isLoggedIn = await authUtil.isLoggedIn();
    if (isLoggedIn)
      this.props.navigation.navigate({
        routeName: "PlaceOrder",
        params: { type }
      });
    else this._resetToScreen({ routeName: "Login" });
  };

  _resetToScreen(params) {
    this.props.navigation.dispatch(
      NavigationActions.reset({
        index: 0,
        actions: [NavigationActions.navigate(params)]
      })
    );
  }

  _renderServie = service => {
    return (
      <View key={service.name} style={styles.serviceBlock}>
        <TouchableOpacity
          style={styles.serviceButton}
          onPress={this._handleClick.bind(this, service.type)}
        >
          <View style={styles.serviceBlockInner}>
            <Image source={service.icon} style={styles.serviceImage} />
          </View>
          <Text weight="bold" level="extra_large" style={styles.serviceName}>
            {service.name}
          </Text>
        </TouchableOpacity>
      </View>
    );
  };
  render() {
    const { isLoading } = this.props;

    return (
      <View style={generalStyles.container.style}>
        <View padder style={generalStyles.content.style}>
          <View style={styles.servicesContainer}>
            {servicesList.map(this._renderServie)}
          </View>
        </View>
      </View>
    );
  }
}

export default ServicesScreen;
