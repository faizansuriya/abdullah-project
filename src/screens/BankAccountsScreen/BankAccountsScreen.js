import React, { Component } from "react";
import { View, Image } from "react-native";

import styles from "./bankAccountsScreenStyle.js";
import * as generalStyles from "../../styles/generalStyle";
import locale from "../../resources/strings/ar";
import { Text } from "../../components/ui";
import bankAccountLogo from "../../resources/assets/Mint_Logo.png";

class BankAccountsScreen extends Component {
  render() {
    const { isLoading } = this.props;

    return (
      <View style={generalStyles.container.style}>
        <View padder style={generalStyles.content.style}>
          <View style={styles.baContainer}>
            <Image
              source={bankAccountLogo}
              style={{
                width: 250,
                height: 250,
                resizeMode: Image.resizeMode.contain
              }}
            />
            <Text style={{color :"black"}}level="XXL" weight="bold">
              قريباً
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

export default BankAccountsScreen;
