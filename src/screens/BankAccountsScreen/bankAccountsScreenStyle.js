import { StyleSheet } from "react-native";
import { Image } from "react-native";

export default StyleSheet.create({
  baContainer: {
    flex: 1,
    flexDirection: "column",
    flexWrap: "wrap",
    padding: 5,
    alignItems: "center",
    justifyContent: "flex-start"
  },
  baRow: {
    margin: 5,
    color: "black",
    textAlign: "right"
  }
});
