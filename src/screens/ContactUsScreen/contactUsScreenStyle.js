import { StyleSheet } from "react-native";
import { Image } from "react-native";

export default StyleSheet.create({
  cuContainer: {
    flex: 1,
    flexDirection: "column",
    flexWrap: "wrap"
  },
  cuRow: {
    marginVertical: 10,
    padding : 20,
    color: "black",
    textAlign: "right"
  }
});
