import React, { Component } from "react";
import { View, Image } from "react-native";

import styles from "./contactUsScreenStyle.js";
import * as generalStyles from "../../styles/generalStyle";
import locale from "../../resources/strings/ar";
import { Text } from "../../components/ui";

import contactUsIcon from "../../resources/assets/icons/contact-icon.png";
class ContactUsScreen extends Component {
  render() {
    const { isLoading } = this.props;

    return (
      <View style={generalStyles.container.style}>
        <View padder style={generalStyles.content.style}>
          <View style={styles.cuContainer}>
            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                marginTop: 40
              }}
            >
              <Image
                source={contactUsIcon}
                style={{
                  width: 100,
                  height: 100,
                  resizeMode: Image.resizeMode.contain
                }}
              />
              <Text
                weight={"bold"}
                level={"large"}
                loose={"loose"}
                style={styles.cuRow}
              >
                يسعدنا تواصلكم معنا لأي إستفسار من خلال بيانات التواصل التالية:
              </Text>
            </View>

            <View
              style={{
                flexDirection: "column",
                height: 50,
                margin: 10,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text
                style={{ color: "black" }}
                weight={"bold"}
                loose={"loose"}
                level={"large"}
              >
                رقم الإتصال الموحد
              </Text>
              <Text
                style={{ color: "black" }}
                weight={"bold"}
                loose={"loose"}
                level={"huge"}
              >
                920010318
              </Text>
            </View>
            <View
              style={{
                flexDirection: "column",
                height: 50,
                margin: 10,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text style={{ color: "black" }} weight={"bold"} level={"large"}>
                أو على رقم الجوال
              </Text>
              <Text style={{ color: "black" }} weight={"bold"} level={"huge"}>
                0559210317
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default ContactUsScreen;
