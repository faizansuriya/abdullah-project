import React, { Component } from "react";
import { View, Image, Linking, TouchableOpacity } from "react-native";

import styles from "./developedByScreenStyle.js";
import * as generalStyles from "../../styles/generalStyle";
import locale from "../../resources/strings/ar";
import { Text } from "../../components/ui";
const _appImage = require("../../resources/assets/splash.png");
const _sa3dImage = require("../../resources/assets/sa3d.png");

class DevelopedByScreen extends Component {
  render() {
    const { isLoading } = this.props;
    const web = "https://saadapps.com/";

    return (
      <View style={generalStyles.container.style}>
        <View padder style={generalStyles.content.style}>
          <View style={styles.developedByContainer}>
            <View style={styles.appNameImgContainer}>
              <Image source={_appImage} style={styles.appNameImg} />
            </View>
            <View style={styles.secondHalfContainer}>
              <View style={{ flex: 1, justifyContent: "space-between" }}>
                <View>
                  <Text style={styles.text1}>تطبيق المساند الأفضل</Text>
                  <Text style={styles.text2}>الإصدار ١٫٠</Text>
                </View>
                <Text style={styles.text3}>برمجة وتصميم:</Text>
              </View>
              <View style={styles.sa3dImgContainer}>
                <Image source={_sa3dImage} style={styles.sa3dImg} />
              </View>
              <TouchableOpacity
                onPress={() => {
                  Linking.openURL(web);
                  console.log("heeeloooo", web);
                }}
              >
                <Text style={styles.linkText}>www.saadapps.com</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default DevelopedByScreen;
