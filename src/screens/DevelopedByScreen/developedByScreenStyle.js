import { StyleSheet } from "react-native";
import { Image } from "react-native";

export default StyleSheet.create({
  developedByContainer: {
    flex: 1,
    flexDirection: "column",
    flexWrap: "wrap",
    padding: 20
  },
  appNameImgContainer: {
    flex: 1,
    alignItems: "center",
    paddingLeft: 10
  },

  appNameImg: {
    width: "100%",
    height: "100%",
    resizeMode: Image.resizeMode.contain
  },
  secondHalfContainer: {
    flex: 2,
    flexDirection: "column",
    flexWrap: "wrap",
    padding: 40
  },

  sa3dImgContainer: {
    flex: 1,
    alignItems: "center",
    paddingLeft: 20,
    paddingRight: 20
  },
  sa3dImg: {
    width: "100%",
    height: "100%",
    resizeMode: Image.resizeMode.contain
  },
  linkText: {
    color: "#503f56",
    fontSize: 20,
    fontWeight: "900",
    textAlign: "center"
  },
  text1: {
    color: "black",
    fontSize: 20,
    fontWeight: "900",
    textAlign: "center"
  },
  text2: {
    color: "#a58e8e",
    fontSize: 12,
    textAlign: "center"
  },
  text3: {
    color: "#4a8597",
    fontSize: 20,
    fontWeight: "900",
    textAlign: "center"
  }
});
