import { StyleSheet } from "react-native";
import Colors from "../../resources/colors";

const linkColor = "#2b0fda";
export default StyleSheet.create({ 
    label: {
        marginBottom: 5,
        marginTop: 2
      },
      btn: {
        marginTop : 20,
        marginBottom: 20,
        backgroundColor: Colors.Black,
        borderWidth: 1,
        borderColor: "#ebebeb",
        paddingVertical: 13,
        marginHorizontal: 5,
        marginRight : 5,
      },
 });
