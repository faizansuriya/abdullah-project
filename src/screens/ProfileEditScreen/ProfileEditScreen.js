import React, { Component } from "react";
import { Switch, View, TouchableHighlight, Alert } from "react-native";

import styles from "./profileEditScreenStyle.js";
import * as generalStyles from "../../styles/generalStyle";
import locale from "../../resources/strings/ar";
import * as userUtil from "../../utils/user.util";
import { Text, Button, TextInput, Label } from "../../components/ui";
import { requestParseError } from "../../utils/general.util";
import services from "../../services";
import _ from "lodash";

const userKeys = ["name", "email", "city"];
class ProfileEditScreen extends Component {
  state = _.pick(this.props.navigation.state.params.user, userKeys);

  _handleOnChange = (key, val) => {
    this.setState(() => ({ [key]: val }));
  };

  _handleSubmit = async () => {
    const postBody = { ...this.state };
    try {
      const user = await services.user.update(postBody);
      await userUtil.setUserInfo(user);
      this.props.navigation.state.params.callback(user);
      this.props.navigation.pop();
    } catch (ex) {
      const _msg = requestParseError(ex);
      Alert.alert("تنبيه", _msg);
    }
  };

  render() {
    const { isLoading } = this.props;
    const { name, email, city } = this.state;
    return (
      <View style={generalStyles.container.style}>
        <View padder style={generalStyles.content.style}>
          <View style={generalStyles.form.style}>
            <View regular style={[generalStyles.controlMargin.style]}>
              <Label level="large" style={styles.label}>
                {locale.editProfile.name}
              </Label>
              <TextInput
                autoFocus
                value={name}
                onChangeText={this._handleOnChange.bind(this, "name")}
              />
            </View>

            <View regular style={[generalStyles.controlMargin.style]}>
              <Label level="large" style={styles.label}>
                {locale.editProfile.email}
              </Label>
              <TextInput
                value={email}
                onChangeText={this._handleOnChange.bind(this, "email")}
              />
            </View>

            <View regular style={[generalStyles.controlMargin.style]}>
              <Label level="large" style={styles.label}>
                {locale.editProfile.city}
              </Label>
              <TextInput
                value={city}
                onChangeText={this._handleOnChange.bind(this, "city")}
              />
            </View>

            <Button
              containerStyle={styles.btn}
              block
              onPress={this._handleSubmit}
              label={locale.editProfile.submit}
              labelWeight={"bold"}
              labelLevel={"huge"}
            />
          </View>
        </View>
      </View>
    );
  }
}

export default ProfileEditScreen;
