import React, { Component } from "react";
import { View, Alert } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import styles from "./registrationScreenStyle.js";
import * as generalStyles from "../../styles/generalStyle";
import locale from "../../resources/strings/ar";
import { Text, Button, TextInput, Label } from "../../components/ui";
import services from "../../services";
import { requestParseError } from "../../utils/general.util";

const _devDefaultState = {
  email: "user1@almosned.com",
  name: "سعد سالم الحربي",
  city: "بريدة",
  mobile: "",
  password: "123456",
  confirmpassword: "123456"
};

const _defaultState = {
  email: "",
  name: "",
  city: "",
  mobile: "",
  password: "",
  confirmpassword: ""
};

class RegistrationScreen extends Component {
  state = __DEV__ ? _devDefaultState : _defaultState;

  _handleOnChange = (key, val) => {
    this.setState(() => ({ [key]: val }));
  };

  _handleSignup = async () => {
    const postBody = { ...this.state };
    try {
      const user = await services.user.register(postBody);
      this.props.navigation.navigate("MobileConfirmation", { user });
    } catch (ex) {
      let _msg = requestParseError(ex);
      if (locale.registration[_msg]) _msg = locale.registration[_msg];
      Alert.alert("تنبيه", _msg);
    }
  };

  _handleShowTnc = () => {};
  render() {
    const { isLoading } = this.props;
    const { name, email, mobile, city, password, confirmpassword } = this.state;
    return (
      <KeyboardAwareScrollView
        style={{ flex: 1, backgroundColor: "white" }}
        contentStyle={generalStyles.container.style}
        contentContainerStyle={{ backgroundColor: "white", paddingBottom: 25 }}
      >
        <View padder style={generalStyles.content.style}>
          <View style={generalStyles.form.style}>
            <View style={styles.fieldStyle}>
              <Label style={styles.labelStyle} level="large">
                {locale.registration.name}
              </Label>
              <TextInput
                containerStyle={styles.textInputStyle}
                autoFocus
                value={name}
                onChangeText={this._handleOnChange.bind(this, "name")}
              />
            </View>

            <View style={styles.fieldStyle}>
              <Label style={styles.labelStyle} level="large">
                {locale.registration.email}
              </Label>
              <TextInput
                keyboardType={"email-address"}
                containerStyle={styles.textInputStyle}
                value={email}
                onChangeText={this._handleOnChange.bind(this, "email")}
              />
            </View>

            <View style={styles.fieldStyle}>
              <Label style={styles.labelStyle} level="large">
                {locale.registration.mobile}
              </Label>

              <View style={styles.textInputStyle}>
                <TextInput
                  keyboardType={"numeric"}
                  containerStyle={{ flex: 1 }}
                  value={mobile}
                  placeholder={"966XXXXXXXX"}
                  placeholderTextColor={"#C3C3C3"}
                  onChangeText={this._handleOnChange.bind(this, "mobile")}
                />
              </View>
            </View>

            <View style={styles.fieldStyle}>
              <Label style={styles.labelStyle} level="large">
                {locale.registration.city}
              </Label>
              <TextInput
                containerStyle={styles.textInputStyle}
                value={city}
                onChangeText={this._handleOnChange.bind(this, "city")}
              />
            </View>

            <View style={styles.fieldStyle}>
              <Label style={styles.labelStyle} level="large">
                {locale.registration.password}
              </Label>
              <TextInput containerStyle={{ height: 0 }} />

              <TextInput
                containerStyle={styles.textInputStyle}
                secureTextEntry={true}
                value={password}
                onChangeText={this._handleOnChange.bind(this, "password")}
              />
            </View>

            <View style={styles.fieldStyle}>
              <Label style={styles.labelStyle} level="large">
                {locale.registration.confirmPassword}
              </Label>
              <TextInput
                containerStyle={styles.textInputStyle}
                secureTextEntry={true}
                value={confirmpassword}
                onChangeText={this._handleOnChange.bind(
                  this,
                  "confirmpassword"
                )}
              />
            </View>

            <Button
              containerStyle={styles.btnSignup}
              block
              onPress={this._handleSignup}
              label={locale.registration.next}
              labelWeight={"bold"}
              labelLevel={"huge"}
            />
          </View>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

export default RegistrationScreen;
