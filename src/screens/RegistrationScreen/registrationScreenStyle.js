import { StyleSheet } from "react-native";
import Colors from "../../resources/colors";

const inactiveColor = "#dedbe3";
const linkColor = "#2b0fda";
export default StyleSheet.create({
  tncLinkText: {
    textDecorationLine: "underline",
    color: linkColor,
    marginRight: 8
  },
  labelStyle: { marginBottom: 5, marginRight: 5, textAlign: "right" },
  fieldStyle: { margin: 5 },
  btnSignup: {
    marginTop: 20,
    marginBottom: 20,
    backgroundColor: Colors.Black,
    borderWidth: 1,
    borderColor: "#ebebeb",
    paddingVertical: 13,
    marginHorizontal: 5,
    marginRight: 5
  },

  textInputStyle: {
    borderColor: inactiveColor,
    marginRight: 5,
    marginHorizontal: 3,
    borderBottomWidth: 2
  }
});
