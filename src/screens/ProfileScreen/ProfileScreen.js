import React, { Component } from "react";

import { View } from "react-native";

import styles from "./profileScreenStyle.js";

import locale from "../../resources/strings/ar";
import { withNavigationFocus } from "react-navigation";
import * as userUtil from "../../utils/user.util";
import services from "../../services";
import _ from "lodash";

import {
  ProfileHeader,
  ProfileInfo,
  ProfileOrdersCount
} from "../../components/ProfileScreenComps";

const _defaultOrdersCount = {
  currentOrders: 0,
  completedOrders: 0,
  allOrders: 0
};
class ProfileScreen extends Component {
  state = {
    userInfo: {},
    ordersCount: {}
  };


 async componentDidMount(){
       const userInfo = await userUtil.getUserInfo();
      this.setState(() => ({ userInfo }));

  }

  async componentWillReceiveProps(nextProps) {
    const _forFirstTimeActive = nextProps.isFocused && this.props.isFocused;
    const _forEveryTimeActive = nextProps.isFocused !== this.props.isFocused;
    if (_forFirstTimeActive || _forEveryTimeActive) {
    //  const userInfo = await userUtil.getUserInfo();
    //  this.setState(() => ({ userInfo }));
      try {
        const data = await this.fetchOrdersCount();
        this.setState(() => ({ ordersCount: data }));
      } catch (ex) {
        // const _msg = requestParseError(ex);
        // Alert.alert("تنبيه", _msg);
      }
    }
  }

  async fetchOrdersCount() {
    const response = await services.order.getCounts();
    const { data = {} } = response;
    const { processing = 0, completed = 0, wfmt = 0 } = data;
    console.log(data);
    return {
      currentOrders: _.sum([+processing, +wfmt]),
      completedOrders: +completed,
      allOrders: _.sum([+processing, +wfmt, +completed])
    };
  }

  _handleOnEdit = () => {
    const { userInfo } = this.state;
    this.props.navigation.navigate("ProfileEdit", { user: userInfo , callback :(userInfo)=>{
     this.setState({userInfo})
    }});
  };

  updateUserDetails = userInfo => {
    this.setState({ userInfo });
  };
  render() {
    const { userInfo = {}, ordersCount = _defaultOrdersCount } = this.state;
    return (
      <View style={styles.profileContainer}>
        <ProfileHeader
          data={userInfo}
          onEdit={!userInfo.id ? null : this._handleOnEdit}
          locale={locale}
          
        />
        <ProfileOrdersCount locale={locale} data={ordersCount} />
        <ProfileInfo locale={locale} data={userInfo} />
      </View>
    );
  }
}

export default withNavigationFocus(ProfileScreen);
