import { StyleSheet } from "react-native";
import Colors from "../../resources/colors";

const linkColor = "#2b0fda";
export default StyleSheet.create({
  tncLinkText: {
    textDecorationLine: "underline",
    color: linkColor
  },
  descriptionInput: {
    textAlignVertical: "top",
    height: 100,
    borderWidth: 1,
    borderColor: "#eeeaea",
    textAlign: "right"
  },
  label: {
    marginBottom: 5,
    marginTop: 2
  },
  btn: {
    marginTop: 20,
    marginBottom: 20,
    backgroundColor: Colors.Black,
    borderWidth: 1,
    borderColor: "#ebebeb",
    paddingVertical: 13,
    marginHorizontal: 5,
    marginRight: 5
  },
  seperator: {
    marginTop: 5,
    borderTopWidth: 2,
    borderTopColor: "#ebebeb",
    marginBottom: 10
  },
  mapContainer: {
    backgroundColor: "lightgrey",
    height: 300,
    width: "100%",
    justifyContent: "flex-end",
    alignItems: "center"
  },
  map: { ...StyleSheet.absoluteFillObject }
});
