import React, { Component } from "react";
import { View, Dimensions, PermissionsAndroid, Alert } from "react-native";
import MapView, { Marker, ProviderPropType } from "react-native-maps";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import styles from "./placeOrderScreenStyle.js";
import * as generalStyles from "../../styles/generalStyle";
import locale from "../../resources/strings/ar";
import { Text, Button, TextInput, DatetimePicker } from "../../components/ui";
import { Label, RadioFormWrap } from "../../components/ui";

import services from "../../services";
import * as location from "../../utils/location";

const executionTypes = [
  { label: locale.order.timeOptAsap, value: "asap" },
  { label: locale.order.timeOptSpecified, value: "specified" }
];
const { width, height } = Dimensions.get("window");

const ASPECT_RATIO = width / 150;
const LATITUDE = 21.42251;
const LONGITUDE = 39.826168;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;

const _initialRegion = {
  latitude: LATITUDE,
  longitude: LONGITUDE,
  latitudeDelta: LATITUDE_DELTA,
  longitudeDelta: LONGITUDE_DELTA
};

const getCurrentLocation = () => {
  return new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(
      position => resolve(position),
      e => reject(e),
      {
        enableHighAccuracy: true
      }
    );
  });
};

class PlaceOrderScreen extends Component {
  region = {
    latitude: LATITUDE,
    longitude: LONGITUDE,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA
  };
  state = {
    showDatePicker: false,
    type: this.props.navigation.state.params.type,
    userLocation: false,
    location: { latitude: LATITUDE, longitude: LONGITUDE },
    execution_option: executionTypes[0].value,
    execution_at: new Date(),
    description: ""
  };

  locationEvent = async position => {
    location.removeLocationListener(this.locationEvent);
    const latitude = position.coords.latitude;
    const longitude = position.coords.longitude;
    const _location = { latitude, longitude };
    this.region = {
      latitude,
      longitude,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA
    };

    await this.setState({ location: _location });
    setTimeout(() => {
      this.refs.map.animateToRegion(this.region, 500);
    }, 1000);
  };
  componentDidMount() {
    location.addLocationListener(this.locationEvent);
  }

  componentWillUnmount() {
    location.removeLocationListener(this.locationEvent);
  }
  requestLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: "Location Permission",
          message: " App needs access to your camera "
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("Permission given");
        return true;
      } else {
        alert("Set your location manually");
      }
    } catch (err) {
      console.warn(err);
    }

    return false;
  };

  componentWillReceiveProps(nextProps) {
    const _type = _.get(this.props, "navigation.state.params.type");
    const _nType = _.get(nextProps, "navigation.state.params.type");
    if (_type !== _nType) this.setState(() => ({ type: _nType }));
  }

  _toggleDatePicker() {
    this.setState(ps => ({ showDatePicker: !ps.showDatePicker }));
  }

  _onChangeCallback = (key, val) => {
    const isExecOptChange =
      key === "execution_option" && val === executionTypes[1].value;
    const isExecAtChange = key === "execution_at";
    if (isExecAtChange || isExecOptChange) this._toggleDatePicker();
  };
  _handleOnChange = (key, val) => {
    this.setState(
      () => ({ [key]: val }),
      () => {
        this._onChangeCallback(key, val);
      }
    );
  };

  _handleSubmit = async () => {
    const { location } = this.state;
    const _location = `${location.latitude},${location.longitude}`;
    const postBody = { ...this.state, location: _location };
    if (!this._validatePost(postBody))
      Alert.alert("تنبيه", "الرجاء كتابة وصف للخدمة");
    else {
      await services.order.createOrder(postBody);
      this.props.navigation.navigate("OrderList");
    }
  };

  _validatePost({ description }) {
    return description && description !== "";
  }

  _handleMarkerMove = e => {
    this.setState({ location: e.nativeEvent.coordinate });
  };

  render() {
    const { isLoading } = this.props;
    const {
      location,
      execution_option,
      execution_at,
      description,
      showDatePicker,
      userLocation
    } = this.state;

    return (
      <KeyboardAwareScrollView
        style={{ flex: 1, backgroundColor: "white" }}
        contentStyle={generalStyles.container.style}
        contentContainerStyle={{ backgroundColor: "white", paddingBottom: 10 }}
      >
        <View style={generalStyles.content.style}>
          <View style={generalStyles.form.style}>
            <View regular style={[generalStyles.controlMargin.style]}>
              <Label level="large" style={styles.label}>
                {locale.order.location}
              </Label>
              <View style={styles.seperator} />
              <View style={styles.mapContainer}>
                <MapView
                  style={styles.map}
                  initialRegion={this.region}
                  ref={"map"}
                >
                  <Marker
                    draggable
                    coordinate={location}
                    onDragEnd={this._handleMarkerMove}
                  />
                </MapView>
              </View>
            </View>

            <Label level="large" style={[styles.label, { marginRight: 5 }]}>
              {locale.order.time}
            </Label>
            <View style={[styles.seperator, { marginHorizontal: 5 }]} />
            <View regular style={[{ alignItems: "flex-end" }]}>
              <RadioFormWrap
                data={executionTypes}
                selected={execution_option}
                onPress={this._handleOnChange.bind(this, "execution_option")}
                extraText={execution_at.toISOString()}
                extraTextShow={execution_option === executionTypes[1].value}
                extraTextWith={executionTypes[1]}
              />
            </View>

            <View regular style={[generalStyles.controlMargin.style]}>
              <Label level="large" style={[styles.label]}>
                {locale.order.description}
              </Label>
              <View style={styles.seperator} />
              <TextInput
                multiline={true}
                selected={{ start: 0, end: 0 }}
                containerStyle={{}}
                style={styles.descriptionInput}
                value={description}
                placeholder={locale.general.writeHere}
                onChangeText={this._handleOnChange.bind(this, "description")}
              />
            </View>

            <Button
              containerStyle={styles.btn}
              block
              onPress={this._handleSubmit}
              label={locale.order.sendOrder}
              labelWeight={"bold"}
              labelLevel={"huge"}
            />
          </View>
        </View>
        {!showDatePicker ? null : (
          <DatetimePicker
            isVisible={true}
            onConfirm={this._handleOnChange.bind(this, "execution_at")}
            onCancel={() => {
              this._toggleDatePicker();
            }}
          />
        )}
      </KeyboardAwareScrollView>
    );
  }
}

PlaceOrderScreen.propTypes = {
  provider: ProviderPropType
};

export default PlaceOrderScreen;
