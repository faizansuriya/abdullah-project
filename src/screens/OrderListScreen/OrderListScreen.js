import React, { Component } from "react";
import { View, Dimensions } from "react-native";
import { TabNavigator, withNavigationFocus } from "react-navigation";
import locale from "../../resources/strings/ar";
import { tabBarStyle } from "./orderListScreenStyle.js";
import _ from "lodash";
import LoginCheckHOC from "../../components/LoginCheckHOC";
import {
  CurrentOrdersTab,
  CompletedOrdersTab
} from "../../components/OrdersScreenComps";

const OrderListTabNav = TabNavigator(
  {
    CurrentOrdersTab: {
      screen: CurrentOrdersTab,
      navigationOptions: ({ navigation }) => {
        return { title: locale.orderList.currentRequests };
      }
    },
    CompletedOrdersTab: {
      screen: CompletedOrdersTab,
      navigationOptions: ({ navigation }) => {
        return { title: locale.orderList.requestsExecuted };
      }
    }
  },
  {
    initialRouteName: "CurrentOrdersTab",
    order: ["CompletedOrdersTab", "CurrentOrdersTab"],
    ...tabBarStyle,
    tabBarPosition: "top",
    animationEnabled: false,
    swipeEnabled: false
  }
);

class OrderListScreen extends Component {
  render() {
    return (
      <LoginCheckHOC {..._.pick(this.props, ["navigation", "isFocused"])}>
        <OrderListTabNav />
      </LoginCheckHOC>
    );
  }
}
export default withNavigationFocus(OrderListScreen);
