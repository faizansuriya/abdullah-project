import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "red",
    alignItems: "center",
    justifyContent: "center"
  }
});

export const tabBarStyle = {
  
  tabBarOptions: {
    activeTintColor: "#4e4d50",
    inactiveTintColor: "#4e4d50",
    labelStyle: { fontSize: 14 },
    // tabStyle: {},
    indicatorStyle: {
      backgroundColor: "#938e9b"
    },
    style: {
      backgroundColor: "#ebebeb",
      elevation: 0,
      borderBottomWidth: 1,
      borderBottomColor: "#dedbe3"
    }
  }
};
