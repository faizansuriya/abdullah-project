import React, { Component } from "react";
import { View } from "react-native";

import styles from "./privacyPolicyScreenStyle.js";
import * as generalStyles from "../../styles/generalStyle";
import locale from "../../resources/strings/ar";
import { Text } from "../../components/ui";

const iconLocation = "../../resources/assets/icons/privacyPolicy";
const ppPoints = [
  "١- عند استخدامك للخدمات المقدمة من خلال هذا التطبيق، سوف يُطلب منك إدخال بيانات معينة مثل اسمك، وبيانات الاتصال الخاصة بك، وموقعك على خرائط قوقل. وسوف نستخدم هذه البيانات للتواصل معك وتقديم خدماتنا لك.",
  "٢- لا يقوم تطبيق «المساند الأفضل» ببيع بياناتك الشخصية إلى جهات أخرى أو المتاجرة بها.",
  "٣- نقوم باتخاذ الخطوات المناسبة لحماية المعلومات الشخصية التي تزودنا بها.وعند قيامك بطلب جديد أو إدخال بياناتك الشخصية عبر تطبيقنا ، فإنه يتم نقل هذه البيانات عبر الإنترنت بطريقة آمنة.",
  "٤- في حالة ما إذا قررنا تغيير سياسة الخصوصية الخاصة بنا، سنقوم بنشر هذه التغييرات على تطبيقاتنا بحيث تكون دائماً على دراية بأحدث التغييرات.",
  "٥- إستخدامك للتطبيق يعني موافقتك على سياسة الخصوصية الخاصة بنا."
];

class privacyPolicyScreen extends Component {
  render() {
    const { isLoading } = this.props;

    return (
      <View style={generalStyles.container.style}>
        <View padder style={generalStyles.content.style}>
          <View style={styles.privacyPolicyContainer}>
            {ppPoints.map((row, i) => (
              <Text
                weight={"bold"}
                level={"large"}
                style={styles.ppRow}
                key={i}
              >
                {row}
              </Text>
            ))}
          </View>
        </View>
      </View>
    );
  }
}

export default privacyPolicyScreen;
