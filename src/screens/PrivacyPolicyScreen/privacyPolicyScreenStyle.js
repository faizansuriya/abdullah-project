import { StyleSheet } from "react-native";
import { Image } from "react-native";

export default StyleSheet.create({
  privacyPolicyContainer: {
    flex: 1,
    flexDirection: "column",
    flexWrap: "wrap",
    padding: 5
  },
  ppRow: {
    margin: 15,
    color: "black",
    textAlign: "right"
  }
});
