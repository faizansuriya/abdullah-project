import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { View } from "react-native";
class MoveToLoginWithResetScreen extends Component {
  _resetToScreen(routeName) {
    this.props.navigation.dispatch(
      NavigationActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName })]
      })
    );
  }

  componentDidMount() {
    this._resetToScreen("Login");
  }

  render() {
    return null;
  }
}

export default MoveToLoginWithResetScreen;
