import { StyleSheet } from "react-native";
import Colors from "../../resources/colors";

export default StyleSheet.create({
  inputControl: {
    padding: 20,
    backgroundColor: "#ebebeb",
    borderRadius: 5,
    textAlign: "right",
    height: 60
  },

  btnLoginContainer: {
    backgroundColor: "transparent",
    borderWidth: 1,
    borderColor: "#ebebeb",
    paddingVertical: 13,
    margin: 10
  },
  btnLoginText: {
    color: "#0d0d0d"
  },

  fpContainer: {
    marginRight: 5,
    paddingVertical: 1,
    marginBottom: 60,
    alignItems: "flex-end"
  },
  labelStyle: { marginBottom: 5, marginRight: 5, textAlign: "right" },

  btnSignup: {
    backgroundColor: Colors.Black,
    borderWidth: 1,
    borderColor: "#ebebeb",
    paddingVertical: 13
  },

  skipLbl: { color: "black", paddingVertical: 7, marginLeft: 5 },
  skipLblImg: {
    height: 15,
    width: 15,
    marginVertical: 10
  }
});
