import React, { Component } from "react";
import { View, Image, Alert, TouchableHighlight } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Text, Button, TextInput, Label } from "../../components/ui";
import styles from "./loginScreenStyle.js";
import * as generalStyles from "../../styles/generalStyle";
import locale from "../../resources/strings/ar";

import services, { updateHeaders } from "../../services";
import * as authUtil from "../../utils/auth.util";
import { requestParseError } from "../../utils/general.util";
import * as userUtil from "../../utils/user.util";

import skipIcon from "../../resources/assets/icons/back_arrow_grey.png";
import EmailLoginLabelIcon from "../../resources/assets/icons/login/email.png";
import PasswordLoginLabelIcon from "../../resources/assets/icons/login/password.png";
import { NavigationActions } from "react-navigation";
class LoginScreen extends Component {
  state = {
    email: __DEV__ ? "user1@almosned.com" : "",
    password: __DEV__ ? "123456" : "",
    rememberMe: false
  };

  async componentDidMount() {
    const loginStatus = await authUtil.isLoggedIn();
    const token = await authUtil.getToken();
    updateHeaders(token);
    if (loginStatus) this.props.navigation.navigate("Services");
  }

  _handleOnChange = (key, val) => {
    this.setState(() => ({ [key]: val }));
  };

  _resetToScreen(routeName) {
    this.props.navigation.dispatch(
      NavigationActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName })]
      })
    );
  }

  _handleLogin = async () => {
    try {
      const { token, user } = await services.auth.login(this.state);
      updateHeaders(token);
      await authUtil.setToken(token);
      await userUtil.setUserInfo(user);
      this._resetToScreen("Services");
    } catch (ex) {
      // const _msg = requestParseError(ex);
      Alert.alert("تنبيه", "الرجاء التأكد من صحة الإيميل أو الرقم السري");
    }
  };

  _handleSignup = () => {
    this.props.navigation.navigate("Registration");
  };

  _handleOnSkip = () => {
    this._resetToScreen("Services");
  };

  _handleForgotPassword = () => {
    this.props.navigation.navigate("ForgotPassword");
  };

  render() {
    const { isLoading, navigation = {} } = this.props;

    const { email, password, rememberMe } = this.state;

    return (
      <KeyboardAwareScrollView
        style={{ flex: 1, backgroundColor: "white" }}
        contentStyle={[generalStyles.container.style]}
        contentContainerStyle={{ backgroundColor: "white", paddingBottom: 25 }}
      >
        <View padder style={[generalStyles.content.style]}>
          <View style={generalStyles.form.style}>
            <View style={generalStyles.controlMargin.style}>
              <Label style={styles.labelStyle} level="large">
                {locale.login.email}
              </Label>

              <View
                style={[
                  generalStyles.controlMargin.style,
                  {
                    backgroundColor: "#ebebeb",
                    flexDirection: "row-reverse",
                    borderRadius: 5
                  }
                ]}
              >
                <TextInput
                  containerStyle={{ flex: 1 }}
                  style={[styles.inputControl]}
                  icon={EmailLoginLabelIcon}
                  value={email}
                  placeholder={locale.login.email}
                  onChangeText={this._handleOnChange.bind(this, "email")}
                />

                <View
                  style={{
                    width: 60,
                    justifyContent: "center",
                    alignItems: "center",
                    padding: 5
                  }}
                >
                  <Image
                    style={{
                      width: 30,
                      height: 30,
                      resizeMode: Image.resizeMode.contain
                    }}
                    source={EmailLoginLabelIcon}
                  />
                </View>
              </View>
            </View>
            <View style={generalStyles.controlMargin.style}>
              <Label style={styles.labelStyle} level="large">
                {locale.login.password}
              </Label>

              <View
                style={[
                  generalStyles.controlMargin.style,
                  {
                    backgroundColor: "#ebebeb",
                    flexDirection: "row-reverse",
                    borderRadius: 5
                  }
                ]}
              >
                <TextInput
                  containerStyle={{ flex: 1 }}
                  style={[styles.inputControl]}
                  secureTextEntry={true}
                  icon={PasswordLoginLabelIcon}
                  value={password}
                  placeholder={locale.login.password}
                  onChangeText={this._handleOnChange.bind(this, "password")}
                />
                <View
                  style={{
                    width: 60,
                    justifyContent: "center",
                    alignItems: "center",
                    padding: 5
                  }}
                >
                  <Image
                    style={{
                      width: 30,
                      height: 30,
                      resizeMode: Image.resizeMode.contain
                    }}
                    source={PasswordLoginLabelIcon}
                  />
                </View>
              </View>
            </View>
            <Button
              containerStyle={[
                generalStyles.controlMargin.style,
                styles.btnLoginContainer
              ]}
              block
              onPress={this._handleLogin}
              label={locale.login.signin}
              labelWeight={"bold"}
              labelLevel={"huge"}
            />
            <View regular style={styles.fpContainer}>
              <Label
                style={[generalStyles.underline.style, { marginRight: 5 }]}
                onPress={this._handleForgotPassword}
              >
                {locale.login.forgotPassword}
              </Label>
            </View>
          </View>

          <View regular style={[generalStyles.controlMargin.style]}>
            <Button
              containerStyle={[
                generalStyles.controlMargin.style,
                styles.btnSignup
              ]}
              onPress={this._handleSignup}
              label={locale.login.signup}
              labelStyle={styles.signUpLbl}
              labelWeight={"bold"}
              labelLevel={"huge"}
            />
          </View>

          <TouchableHighlight
            underlayColor={"transparent"}
            activeOpacity={0.7}
            onPress={this._handleOnSkip}
            style={{ margin: 5, padding: 5 }}
          >
            <View style={{ flexDirection: "row" }}>
              <Image source={skipIcon} style={styles.skipLblImg} />
              <Label style={styles.skipLbl}>{locale.login.skip}</Label>
            </View>
          </TouchableHighlight>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

export default LoginScreen;
