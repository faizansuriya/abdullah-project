import { StyleSheet } from "react-native";
import Colors from "../../resources/colors";
const ty  = StyleSheet.create({
  inputControl: {
    padding: 20,
    backgroundColor: "#ebebeb",
    borderRadius: 5,
    textAlign: "right",
    height: 60
  },

  btnLoginContainer: {
    backgroundColor: Colors.Black,
    borderWidth: 1,
    borderColor: "#ebebeb",
    paddingVertical: 13,
    margin: 10
  },
  btnSignUpText: {
    color: "black"
  },

  fpContainer: {
    marginRight: 5,
    paddingVertical: 1,
    marginBottom: 60,
    alignItems: "flex-end"
  },
  labelStyle: { marginBottom: 5, marginRight: 5, textAlign: "right" },

  btnSignup: {
    backgroundColor: "#ebebeb",
    borderWidth: 1,
    borderColor: "#ebebeb",
    paddingVertical: 13
  },

  skipLbl: { color: "black", paddingVertical: 7, marginLeft: 5 },
  skipLblImg: {
    height: 15,
    width: 15,
    marginVertical: 10
  },
  signUpLbl: {
    color : Colors.Black
  }
});

export default ty;