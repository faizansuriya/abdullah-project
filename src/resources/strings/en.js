const signup = "Register";
export default {
  login: {
    title: "login",
    email: "email",
    password: "password",
    signin: "signin",
    forgotPassword: "forgotPassword",
    signup: signup,
    newUser: "newUser"
  },
  registration: {
    title: signup,
    name: "name:",
    email: "email:",
    emailOpt: "emailOpt:",
    mobile: "mobile:",
    city: "city:",
    password: "password:",
    confirmPassword: "confirmPassword:",
    next: "next:",
    tnc: "tnc:",
    tncLink: "tncLink:"
  },
  profile: {
    title: "account"
  }
};
