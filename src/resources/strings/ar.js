const signup = "تسجيل حساب جديد";
const email = "البريد الإلكتروني";
const mobile = "رقم الجوال:";
const city = "المدينة:";
const name = "الاسم:";
export default {
  login: {
    title: "تسجيل الدخول",
    email: "البريد الإلكتروني",
    password: "الرقم السري",
    signin: "دخول",
    forgotPassword: "نسيت كلمة المرور؟",
    signup: signup,
    newUser: "مستخدم جديد؟",
    skip: "تخطي"
  },
  registration: {
    title: signup,
    name: name,
    emailOpt: "(إختياري):",
    email: email,
    mobile: mobile,
    city: city,
    password: "الرقم السري:",
    confirmPassword: "تأكيد الرقم السري:",
    next: "التالي",
    tnc: `بالتسجيل معنا في تطبيق “المساند الأفضل” فإنك توافق على`,
    tncLink: "شروط الخدمة",
    "Email already exists": "هذا الإيميل مستخدم مسبقاً"
  },
  mobileConfirmation: {
    title: "تأكيد رقم الجوال",
    enterCodeText: "أدخل كود التفعيل الذي تم إرساله لجوالك:",
    register: "التسجيل",
    successMessage: "تم التسجيل بنجاح"
  },
  forgotPassword: {
    title: "إستعادة الرقم السري",
    enterEmailText: `أدخل بريدك الإلكتروني المسجل لدينا حتى نقوم بإرسال
رقمك السري إليه`,
    email: email + ":",
    recover: "إستعادة",
    successMessage: "تم إعادة تعيين الرقم السري بنجاح"
  },
  services: {
    title: "اختر الخدمة التي تريد",
    electricity: "كهرباء",
    carpentry: "نجارة",
    refrigeration: "تبريد وتكييف",
    plumbing: "سباكة",
    paint: "دهان",
    cleaning: "تنظيف"
  },
  order: {
    title: "حدد الموقع والخيارات",
    location: "موقعك (اللوكيشن):",
    time: "وقت تنفيذ الخدمة:",
    timeOptAsap: "في أقرب وقت ممكن",
    timeOptSpecified: "في وقت محدد من إختياري",
    description: "وصف الخدمة:",
    sendOrder: "أرسل الطلب"
  },
  orderList: {
    title: "طلباتي",
    currentRequests: "طلبات حالية",
    requestsExecuted: "طلبات منفذه"
  },
  profile: {
    title: "حسابي",
    currentOrders: "كل الطلبات",
    completedOrders: "طلبات منفذه",
    allOrders: "طلبات حالية",
    email: email,
    mobile: mobile,
    city: city
  },
  editProfile: {
    title: "حسابي",
    name: name + ":",
    email: email + ":",
    city: city,
    submit: "أرسل"
  },
  settings: {
    title: "الإعدادات",
    privacyPolicy: "إتفاقية الإستخدام",
    aboutTheApp: "عن تطبيق “المساند الأفضل”",
    bankAccounts: "أرقام حساباتنا",
    contactUs: "اتصل بنا",
    shareTheApp: "شارك التطبيق مع أصدقائك",
    rateTheApp: "قيم التطبيق في متجر البرامج",
    developedBy: "برمجة وتصميم",
    logout: "تسجيل الخروج",
    login: "تسجيل الدخول"
  },
  review: {
    title: "رأيك يهمنا",
    subTitle:
      "شاركنا تقييمك للخدمة المقدمة لك ، نهدف لتقديم أفضل خدمة لكم دائماً.رأيك سيساعدنا كثيراً لتحقيق هذا الهدف.",
    speed: "كيف كانت سرعة الخدمة:",
    speedPh: "سريعة جداً .. جيدة",
    description: "تقييمك للخدمة بشكل عام:",
    descriptionPh: "أكتب هنا...",
    submit: "أرسل"
  },
  general: {
    writeHere: "أكتب هنا...",
    submit: "أرسل",
    thanksForReview: "شكراً لتقييمك",
    giveReview: "قيم الخدمة الآن",
    cancel: "إلغاء",
    datePickerTitle: "أختر التاريخ والوقت",
    confirm: "تأكيد"
  },
  statuses: {
    processing: "جاري التنفيذ",
    wfmt: "في إنتظار التحويل",
    completed: "طلبات منفذه"
  },
  numbers: {
    "0": "۰",
    "1": "١",
    "2": "٢",
    "3": "٣",
    "4": "٤",
    "5": "٥",
    "6": "٦",
    "7": "٧",
    "8": "٨",
    "9": "٩"
  },
  contactUs: {
    heading: `يسعدنا تواصلكم معنا لأي إستفسار من خلال بيانات التواصل التالية:`,
    contact_1: `رقم الإتصال الموحد`,
    contact_2: ``
  }
};
