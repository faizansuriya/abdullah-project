const Colors = {
  Transparent: "transparent",
  White: "#ffffff",
  TransWhite: "#ffffffdd",
  BlueGreen: "#00caca",
  Mint: "#b1eae4",
  Orange: "#f69871",
  DarkGrey: "#868686",
  LightGrey: "#C8C8C8",
  Khaki: "#8ba39e",
  FilterGrey: "#8C98A3",
  Black: "#4E4D50",

  Pictogram: {
    Yellow: "#ffb504",
    Purple: "#801989",
    Blue: "#0e758b",
    Red: "#a81e38",
    Brown: "#9c6920",
    Pink: "#d734a3",
    Khaki: "#7aa110",
    Orange: "#d56c0f",
    Green: "#146414",
    Grey: "#595959"
  }
};

export default Colors;
