import React, { Component } from "react";
import { YellowBox } from "react-native";

import SplashScreen from "./src/screens/SplashScreen";
import RootStack from "./src/routes";
YellowBox.ignoreWarnings([
  "Warning: isMounted(...) is deprecated",
  "Module RCTImageLoader"
]);

class App extends Component {
  state = { showSplash: true };

  _hideSplashScreen = () => {
    this.setState(() => ({ showSplash: false }));
  };
  componentDidMount() {
    setTimeout(this._hideSplashScreen, 1000);
  }
  render() {
    const { showSplash } = this.state;
    return showSplash ? <SplashScreen /> : <RootStack />;
  }
}

export default App;
